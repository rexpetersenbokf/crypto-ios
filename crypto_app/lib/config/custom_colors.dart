// ignore_for_file: unnecessary_const

import 'package:flutter/material.dart';

class CustomColors {
  static const MaterialColor kAppBlue = const MaterialColor(
    0xff001489,
    const <int, Color>{
      50: const Color(0xff00127b), //10%
      100: const Color(0xff00106e), //20%
      200: const Color(0xff000e60), //30%
      300: const Color(0xff000c52), //40%
      400: const Color(0xff000a45), //50%
      500: const Color(0xff000837), //60%
      600: const Color(0xff000629), //70%
      700: const Color(0xff00041b), //80%
      800: const Color(0xff00020e), //90%
      900: const Color(0xff000000), //100%
    },
  );
}
