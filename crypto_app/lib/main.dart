import 'package:crypto_app/providers/user_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:amazon_cognito_identity_dart_2/cognito.dart';

import 'config/server_config.dart';
import 'config/custom_colors.dart';
import 'config/constant.dart';

import 'providers/signup.dart';
import 'providers/account.dart';
import 'providers/order.dart';

import './screens/splash_screen.dart';
import './screens/welcome_screen.dart';
import './screens/login_screen.dart';
import './screens/dashboard_screen.dart';
import 'screens/errors_screen.dart';
import './screens/unavailable_screen.dart';
import './screens/signup_screen.dart';
import './screens/account_home_screen.dart';
import './screens/buy_bitcoin_screen.dart';
import './screens/quote_screen.dart';
import './screens/buy_confirm_state.dart';
import './screens/transactions_screen.dart';
import './screens/documents_screen.dart';
import './screens/transaction_receipt_screen.dart';

void main() {
  Constants.setEnvironment(Environment.dev);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: UserService(CognitoUserPool(userPoolID, clientID)),
        ),
        ChangeNotifierProvider.value(
          value: Signup(),
        ),
        ChangeNotifierProvider.value(
          value: Account(),
        ),
        ChangeNotifierProvider.value(
          value: Order(),
        ),
      ],
      child: MaterialApp(
        title: 'Crypto',
        theme: ThemeData(
          primarySwatch: CustomColors.kAppBlue,
          fontFamily: 'Roboto',
        ),
        home: const SplashScreen(),
        routes: {
          WelcomeScreen.routeName: (ctx) => WelcomeScreen(),
          LoginScreen.routeName: (ctx) => const LoginScreen(),
          DashboardScreen.routeName: (ctx) => DashboardScreen(),
          ErrorsScreen.routeName: (ctx) => const ErrorsScreen(),
          UnavailableScreen.routeName: (ctx) => const UnavailableScreen(),
          SignupScreen.routeName: (ctx) => SignupScreen(),
          AccountHomeScreen.routeName: (ctx) => const AccountHomeScreen(),
          BuyBitcoinScreen.routeName: (ctx) => const BuyBitcoinScreen(),
          //QuoteScreen.routeName: (ctx) => QuoteScreen(),
          BuyConfirmState.routeName: (ctx) => const BuyConfirmState(),
          TransactionsScreen.routeName: (ctx) => const TransactionsScreen(),
          DocumentScreen.routeName: (ctx) => const DocumentScreen(),
          TransactionReceiptScreen.routeName: (ctx) =>
              TransactionReceiptScreen(),
        },
        onGenerateRoute: (settings) {
          if (settings.name == QuoteScreen.routeName) {
            final args = settings.arguments as Map<String, dynamic>;
            return MaterialPageRoute(
              builder: (context) {
                return QuoteScreen(args);
              },
            );
          }
          // The code only supports
          // PassArgumentsScreen.routeName right now.
          // Other values need to be implemented if we
          // add them. The assertion here will help remind
          // us of that higher up in the call stack, since
          // this assertion would otherwise fire somewhere
          // in the framework.
          assert(false, 'Need to implement ${settings.name}');
          return null;
        },
      ),
    );
  }
}
