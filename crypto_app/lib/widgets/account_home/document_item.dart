import 'package:flutter/material.dart';

import '../../models/document.dart';

class DocumentItem extends StatelessWidget {
  final Document document;
  // ignore: use_key_in_widget_constructors
  const DocumentItem(this.document);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(
            bottom: 8,
          ),
          width: double.infinity,
          height: 1,
          decoration: const BoxDecoration(
            color: Color.fromARGB(
              255,
              189,
              189,
              189,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 8,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    document.type,
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey,
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    document.title,
                    style: const TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ),
              const Icon(
                Icons.download,
                color: Colors.blue,
              ),
            ],
          ),
        ),
        if (document.isLast == true) ...[
          Container(
            margin: const EdgeInsets.only(
              top: 8,
            ),
            width: double.infinity,
            height: 1,
            decoration: const BoxDecoration(
              color: Color.fromARGB(
                255,
                189,
                189,
                189,
              ),
            ),
          ),
        ] else ...[
          const SizedBox(
            height: 8,
          )
        ]
      ],
    );
  }
}
