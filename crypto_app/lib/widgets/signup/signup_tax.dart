import 'package:flutter/material.dart';
import '../../config/constant.dart';

// ignore: must_be_immutable
class SignupTaxCard extends StatefulWidget {
  String taxIdentificationValue;
  void Function(String?)? onTaxIdentificationChange;
  void Function(String?)? onTaxClassificationChange;
  TextEditingController businessNameTextFieldController;
  TextEditingController tinTextFieldController;
  TextEditingController payeeCodeTextFieldController;

  SignupTaxCard(
      this.taxIdentificationValue,
      this.onTaxIdentificationChange,
      this.onTaxClassificationChange,
      this.businessNameTextFieldController,
      this.tinTextFieldController,
      this.payeeCodeTextFieldController,
      {Key? key})
      : super(key: key);

  @override
  _SignupTaxCard createState() => _SignupTaxCard();
}

class _SignupTaxCard extends State<SignupTaxCard> {
  final _taxClassification = taxClasificationDropdown[0];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 240, 240, 240),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 16,
      ),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'My tax classification is...',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 8,
              bottom: 32,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: DropdownButtonFormField(
                    items: identificationDropdown
                        .map((String item) => DropdownMenuItem<String>(
                            child: Text(item), value: item))
                        .toList(),
                    onChanged: widget.onTaxIdentificationChange,
                    value: widget.taxIdentificationValue,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.fromLTRB(10, 14, 10, 14),
                      filled: true,
                      fillColor: Color.fromARGB(255, 240, 240, 240),
                      hintText: 'Choose',
                      errorText: null,
                      border: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Color.fromARGB(255, 240, 240, 240),
                            width: 1.0),
                      ),
                    ),
                  ),
                ),
                IconButton(
                  icon: const Icon(
                    Icons.info_outline,
                    size: 30,
                  ),
                  color: const Color.fromARGB(255, 130, 130, 130),
                  onPressed: () {
                    showCustomDialog(context, 'My tax classification is...',
                        'Complete this information based on the federal tax classification of the person whose name is entered above.\n\nIf you are a sole proprietor or single member LLC, enter your individual name above.\n\nIf you are a disregarded entity, enter the name of the owner (that is not a disregarded entity) above and enter the disregarded entity’s name under “Business Name” below.');
                  },
                ),
              ],
            ),
          ),
          if (widget.taxIdentificationValue != 'Individual') ...[
            if (widget.taxIdentificationValue ==
                'Limited liability company (LLC)') ...[
              Text(
                'LLC Tax Classification',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 8,
                  bottom: 32,
                ),
                child: DropdownButtonFormField(
                  items: taxClasificationDropdown
                      .map((String item) => DropdownMenuItem<String>(
                          child: Text(item), value: item))
                      .toList(),
                  onChanged: widget.onTaxClassificationChange,
                  value: _taxClassification,
                  decoration: const InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(10, 14, 10, 14),
                    filled: true,
                    fillColor: Color.fromARGB(255, 240, 240, 240),
                    hintText: 'Choose',
                    errorText: null,
                    border: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Color.fromARGB(255, 240, 240, 240),
                          width: 1.0),
                    ),
                  ),
                ),
              ),
            ],
            Text(
              'Business Name',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: Theme.of(context).primaryColor,
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(
                top: 2,
              ),
              child: Text(
                'If different from above name',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                  color: Color.fromARGB(255, 92, 92, 92),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 8,
                bottom: 32,
              ),
              child: Row(
                children: [
                  Expanded(
                    child: TextField(
                      decoration: const InputDecoration(
                        floatingLabelBehavior: FloatingLabelBehavior.never,
                        isDense: true,
                        hintText: 'Enter',
                        labelText: 'Enter',
                        border: OutlineInputBorder(),
                      ),
                      autocorrect: false,
                      controller: widget.businessNameTextFieldController,
                    ),
                  ),
                  IconButton(
                    icon: const Icon(
                      Icons.info_outline,
                      size: 30,
                    ),
                    color: const Color.fromARGB(255, 130, 130, 130),
                    onPressed: () {
                      showCustomDialog(context, 'Business Name',
                          'If you have a business name, trade name, DBA name or disregarded entity name, you may enter it here.');
                    },
                  ),
                ],
              ),
            ),
          ],
          Text(
            'Tax Identification Number',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 8,
            ),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: const InputDecoration(
                      floatingLabelBehavior: FloatingLabelBehavior.never,
                      isDense: true,
                      hintText: 'Enter',
                      labelText: 'Enter',
                      border: OutlineInputBorder(),
                    ),
                    keyboardType: TextInputType.number,
                    autocorrect: false,
                    controller: widget.tinTextFieldController,
                  ),
                ),
                IconButton(
                  icon: const Icon(
                    Icons.info_outline,
                    size: 30,
                  ),
                  color: const Color.fromARGB(255, 130, 130, 130),
                  onPressed: () {
                    showCustomDialog(context, 'Tax Identification Number (TIN)',
                        'Enter your Social Security Number (SSN) or Employer Identification Number (EIN).\n\nIf you are a resident alien and you do not have and are not eligible to get an SSN, your TIN is your IRS individual taxpayer identification number (ITIN).\n\nIf you are a sole proprietor and you have an EIN, you may enter either your SSN or EIN.\n\nIf you are a single-member LLC that is disregarded as an entity, enter the owner’s SSN (or EIN, if the owner has one). If the LLC is classified as a corporation or partnership, enter the entity’s EIN.');
                  },
                ),
              ],
            ),
          ),
          if (widget.taxIdentificationValue != 'Individual') ...[
            const SizedBox(
              height: 32,
            ),
            Text(
              'Exempt Payee Code (If Any)',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 16,
                fontWeight: FontWeight.w700,
                color: Theme.of(context).primaryColor,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 8,
              ),
              child: TextField(
                decoration: const InputDecoration(
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  isDense: true,
                  hintText: 'Enter',
                  labelText: 'Enter',
                  border: OutlineInputBorder(),
                ),
                autocorrect: false,
                controller: widget.payeeCodeTextFieldController,
              ),
            ),
          ],
        ],
      ),
    );
  }
}
