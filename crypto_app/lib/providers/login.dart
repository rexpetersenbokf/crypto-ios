import 'package:flutter/widgets.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import '../config/api_endpoints.dart' as api;
import '../config/server_config.dart';

class Login with ChangeNotifier {
  Future<Map<String, dynamic>> loginCustomer(
    String username,
    String password,
  ) async {
    final apiUrl = Uri.parse('${Constants.serverURL}${api.kLoginCustomer}');
    try {
      final response = await http.post(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
        body: json.encode(
          {
            'username': username,
            'password': password,
          },
        ),
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      rethrow;
    }
  }
}
