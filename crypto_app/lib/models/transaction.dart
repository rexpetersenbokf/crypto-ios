class Transaction {
  String transactionId;
  String accountNumber;
  String customerId;
  String transactionType;
  String transactionSubtype;
  String amount;
  String currency;
  String price;
  String priceCurrency;
  String tradeDate;
  String bookingDate;
  String createdAt;
  Trade trade;
  String receipt;
  String createdActor;

  Transaction(
    this.transactionId,
    this.accountNumber,
    this.customerId,
    this.transactionType,
    this.transactionSubtype,
    this.amount,
    this.currency,
    this.price,
    this.priceCurrency,
    this.tradeDate,
    this.bookingDate,
    this.createdAt,
    this.trade,
    this.receipt,
    this.createdActor,
  );

  factory Transaction.fromJson(dynamic json) {
    return Transaction(
      json['transactionId'] as String,
      json['accountNumber'] as String,
      json['customerId'] as String,
      json['transactionType'] as String,
      json['transactionSubtype'] as String,
      json['amount'] as String,
      json['currency'] as String,
      json['price'] as String,
      json['priceCurrency'] as String,
      json['tradeDate'] as String,
      json['bookingDate'] as String,
      json['createdAt'] as String,
      Trade.fromJson(json['trade']),
      json['receipt'] as String,
      json['createdActor'] as String,
    );
  }

  @override
  String toString() {
    return '{ $transactionId, $accountNumber, $customerId, $transactionType, $transactionSubtype, $amount, $currency, $price, $priceCurrency, $tradeDate, $bookingDate, $createdAt, $trade, $receipt, $createdActor }';
  }
}

class Trade {
  String direction;
  Pay pay;
  Receive receive;
  List<Fee>? fees;
  String executedAt;

  Trade(
    this.direction,
    this.pay,
    this.receive,
    this.fees,
    this.executedAt,
  );

  factory Trade.fromJson(dynamic json) {
    if (json['fees'] != null) {
      var feeObjsJson = json['fees'] as List;
      List<Fee> _fees =
          feeObjsJson.map((tagJson) => Fee.fromJson(tagJson)).toList();

      return Trade(
        json['direction'] as String,
        Pay.fromJson(json['pay']),
        Receive.fromJson(json['receive']),
        _fees,
        json['executedAt'] as String,
      );
    } else {
      return Trade(
        json['direction'] as String,
        Pay.fromJson(json['pay']),
        Receive.fromJson(json['receive']),
        null,
        json['executedAt'] as String,
      );
    }
  }

  @override
  String toString() {
    return '{ $direction, $executedAt, $pay, $receive, $fees }';
  }
}

class Pay {
  String currency;
  String amount;
  String subtotal;
  String total;

  Pay(
    this.currency,
    this.amount,
    this.subtotal,
    this.total,
  );

  factory Pay.fromJson(dynamic json) {
    return Pay(
      json['currency'] as String,
      json['amount'] as String,
      json['subtotal'] as String,
      json['total'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount, $subtotal, $total }';
  }
}

class Receive {
  String currency;
  String amount;

  Receive(
    this.currency,
    this.amount,
  );

  factory Receive.fromJson(dynamic json) {
    return Receive(
      json['currency'] as String,
      json['amount'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount }';
  }
}

class Fee {
  String currency;
  String amount;
  String rateMethod;
  String rateValue;
  String feeType;

  Fee(
    this.currency,
    this.amount,
    this.rateMethod,
    this.rateValue,
    this.feeType,
  );

  factory Fee.fromJson(dynamic json) {
    return Fee(
        json['currency'] as String,
        json['amount'] as String,
        json['rateMethod'] as String,
        json['rateValue'] as String,
        json['feeType'] as String);
  }

  @override
  String toString() {
    return '{ $currency, $amount, $rateMethod, $rateValue, $feeType }';
  }
}
