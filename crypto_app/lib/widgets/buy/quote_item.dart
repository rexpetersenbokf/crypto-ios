import 'package:flutter/material.dart';
import '../../models/quote_model.dart';

class QuoteItem extends StatelessWidget {
  final QuoteModel quote;
  // ignore: use_key_in_widget_constructors
  const QuoteItem(this.quote);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          width: double.infinity,
          height: 0.25,
          decoration: const BoxDecoration(
            color: Color.fromARGB(
              255,
              189,
              189,
              189,
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: const EdgeInsets.only(
                top: 18,
                bottom: 18,
              ),
              child: Text(
                quote.title,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: (quote.title == 'Cost' ||
                          quote.title == 'Net Sale Proceeds')
                      ? FontWeight.w700
                      : FontWeight.w400,
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  quote.info,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 16,
                    fontWeight: (quote.title == 'Cost' ||
                            quote.title == 'Net Sale Proceeds')
                        ? FontWeight.w700
                        : FontWeight.w400,
                  ),
                ),
                if (quote.subinfo != null && quote.subinfo != '') ...[
                  Text(
                    quote.subinfo ?? '',
                    textAlign: TextAlign.right,
                    style: const TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ],
              ],
            ),
          ],
        ),
        if (quote.title == 'Cost' || quote.title == 'Bank account') ...[
          Container(
            width: double.infinity,
            height: 0.25,
            decoration: const BoxDecoration(
              color: Color.fromARGB(
                255,
                189,
                189,
                189,
              ),
            ),
          ),
        ],
      ],
    );
  }
}
