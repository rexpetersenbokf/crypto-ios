import 'package:flutter/material.dart';
import '../../config/constant.dart';

// ignore: must_be_immutable
class SignupAddressCard extends StatefulWidget {
  TextEditingController addressTextFieldController;
  TextEditingController cityTextFieldController;
  TextEditingController zipTextFieldController;
  void Function(Map<String, String>?)? onStateChange;

  SignupAddressCard(
    this.addressTextFieldController,
    this.cityTextFieldController,
    this.zipTextFieldController,
    this.onStateChange, {
    Key? key,
  }) : super(key: key);

  @override
  _SignupAddressCard createState() => _SignupAddressCard();
}

class _SignupAddressCard extends State<SignupAddressCard> {
  final _selectedState = stateDropdown[0];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 16,
      ),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Residential Address',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 8,
              bottom: 32,
            ),
            child: TextField(
              decoration: const InputDecoration(
                floatingLabelBehavior: FloatingLabelBehavior.never,
                isDense: true,
                hintText: 'Address',
                labelText: 'Address',
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.streetAddress,
              autocorrect: false,
              controller: widget.addressTextFieldController,
            ),
          ),
          Text(
            'City',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 8,
            ),
            child: TextField(
              decoration: const InputDecoration(
                floatingLabelBehavior: FloatingLabelBehavior.never,
                isDense: true,
                hintText: 'City',
                labelText: 'City',
                border: OutlineInputBorder(),
              ),
              keyboardType: TextInputType.none,
              autocorrect: false,
              controller: widget.cityTextFieldController,
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Container(
                  padding: const EdgeInsets.only(
                    top: 32,
                    right: 4,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'State',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(
                          top: 8,
                        ),
                      ),
                      DropdownButtonFormField(
                        items: stateDropdown
                            .map((item) =>
                                DropdownMenuItem<Map<String, String>>(
                                    child: Text(item['name'] as String),
                                    value: item))
                            .toList(),
                        onChanged: widget.onStateChange,
                        value: _selectedState,
                        decoration: const InputDecoration(
                          contentPadding: EdgeInsets.fromLTRB(10, 14, 10, 14),
                          filled: true,
                          fillColor: Color.fromARGB(255, 255, 255, 255),
                          hintText: 'Choose',
                          errorText: null,
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color.fromARGB(255, 240, 240, 240),
                                width: 1.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 5,
                child: Container(
                  padding: const EdgeInsets.only(
                    top: 32,
                    left: 4,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Zip',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 16,
                          fontWeight: FontWeight.w700,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(
                          top: 8,
                        ),
                      ),
                      TextField(
                        decoration: const InputDecoration(
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          isDense: true,
                          hintText: 'Zip',
                          labelText: 'Zip',
                          border: OutlineInputBorder(),
                        ),
                        keyboardType: TextInputType.none,
                        controller: widget.zipTextFieldController,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
