import 'package:flutter/widgets.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../config/api_endpoints.dart' as api;
import '../config/server_config.dart';

class Signup with ChangeNotifier {
  Future<Map<String, dynamic>> createCustomer(
      String classification,
      String stateOfResidenceOrIncorporation,
      bool subjectBackupWithholding,
      String ref) async {
    final apiUrl = Uri.parse('${Constants.serverURL}${api.kCreateCustomer}');
    try {
      final response = await http.post(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
        body: json.encode(
          {
            'classification': classification,
            'stateOfResidenceOrIncorporation': stateOfResidenceOrIncorporation,
            'subjectBackupWithholding': subjectBackupWithholding,
            'ref': ref,
          },
        ),
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      rethrow;
    }
  }

  Future<Map<String, dynamic>> createAccount(
    String customerId,
    String masterAccountNumber,
    String ref,
  ) async {
    final apiUrl = Uri.parse('${Constants.serverURL}${api.kCreateAccount}');
    try {
      final response = await http.post(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
        body: json.encode(
          {
            'customerId': customerId,
            'masterAccountNumber': masterAccountNumber,
            'ref': ref,
          },
        ),
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      //print(error.toString());
      rethrow;
    }
  }

  Future<String?> checkAuthenticationStatus() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey('accountNumber')) {
      return null;
    }

    final accountNumber = prefs.getString('accountNumber');
    notifyListeners();
    return accountNumber;
  }
}
