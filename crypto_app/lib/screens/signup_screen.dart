import 'package:flutter/material.dart';
import '../config/constant.dart';

import '../widgets/signup/signup_name.dart';
import '../widgets/signup/signup_tax.dart';
import '../widgets/signup/signup_address.dart';
import '../widgets/signup/signup_instruction.dart';
import '../widgets/signup/signup_terms.dart';
import '../widgets/signup/signup_info.dart';
import '../widgets/signup/signup_button.dart';

class SignupScreen extends StatelessWidget {
  SignupScreen({Key? key}) : super(key: key);
  static const routeName = '/signup';

  final ValueNotifier<String> _taxIdentificationValueChanged =
      ValueNotifier(identificationDropdown[0]);
  final ValueNotifier<String> _taxClassificationValueChanged =
      ValueNotifier(taxClasificationDropdown[0]);
  final ValueNotifier<Map<String, String>> _stateValueChanged =
      ValueNotifier(stateDropdown[0]);
  final ValueNotifier<bool> _isDeclarationCheckboxChanged =
      ValueNotifier(false);
  final ValueNotifier<bool> _isTermsCheckboxChanged = ValueNotifier(false);
  final ValueNotifier<dynamic> _isSignupFormUpdated = ValueNotifier('');

  static TextEditingController nameTextFieldController =
      TextEditingController();
  static TextEditingController businessNameTextFieldController =
      TextEditingController();
  static TextEditingController tinTextFieldController = TextEditingController();
  static TextEditingController payeeCodeTextFieldController =
      TextEditingController();
  static TextEditingController addressTextFieldController =
      TextEditingController();
  static TextEditingController cityTextFieldController =
      TextEditingController();
  static TextEditingController zipTextFieldController = TextEditingController();

  void onTaxIdentificationChange(String? value) {
    _taxIdentificationValueChanged.value = value as String;
    _isSignupFormUpdated.value = value;
  }

  void onTaxClassificationChange(String? value) {
    _taxClassificationValueChanged.value = value as String;
    _isSignupFormUpdated.value = value;
  }

  void onStateChange(Map<String, String>? value) {
    _stateValueChanged.value = value as Map<String, String>;
    _isSignupFormUpdated.value = value;
  }

  void onCheckboxDeclarationChange(bool? value) {
    _isDeclarationCheckboxChanged.value = value ?? false;
    _isSignupFormUpdated.value = value;
  }

  void onCheckboxTermsChange(bool? value) {
    _isTermsCheckboxChanged.value = value ?? false;
    _isSignupFormUpdated.value = value;
  }

  @override
  Widget build(BuildContext context) {
    nameTextFieldController.addListener(() {
      _isSignupFormUpdated.value = nameTextFieldController.text;
    });

    businessNameTextFieldController.addListener(() {
      _isSignupFormUpdated.value = businessNameTextFieldController.text;
    });

    tinTextFieldController.addListener(() {
      _isSignupFormUpdated.value = tinTextFieldController.text;
    });

    payeeCodeTextFieldController.addListener(() {
      _isSignupFormUpdated.value = payeeCodeTextFieldController.text;
    });

    addressTextFieldController.addListener(() {
      _isSignupFormUpdated.value = addressTextFieldController.text;
    });

    cityTextFieldController.addListener(() {
      _isSignupFormUpdated.value = cityTextFieldController.text;
    });

    zipTextFieldController.addListener(() {
      _isSignupFormUpdated.value = zipTextFieldController.text;
    });

    return Scaffold(
      appBar: AppBar(
        title: const Text('Future Bank'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 16,
                ),
                child: Text(
                  'Sign Up',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 28,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.symmetric(
                  vertical: 16,
                  horizontal: 16,
                ),
                child: Text(
                  'You must provide the following information and certification to NYDIG to proceed.',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
              SignupNameCard(nameTextFieldController),
              ValueListenableBuilder(
                  valueListenable: _taxIdentificationValueChanged,
                  builder: (context, value, child) {
                    return SignupTaxCard(
                        value as String,
                        onTaxIdentificationChange,
                        onTaxClassificationChange,
                        businessNameTextFieldController,
                        tinTextFieldController,
                        payeeCodeTextFieldController);
                  }),
              ValueListenableBuilder(
                  valueListenable: _stateValueChanged,
                  builder: (context, value, child) {
                    return SignupAddressCard(
                      addressTextFieldController,
                      cityTextFieldController,
                      zipTextFieldController,
                      onStateChange,
                    );
                  }),
              ValueListenableBuilder(
                valueListenable: _isDeclarationCheckboxChanged,
                builder: (context, value, child) {
                  return SignupInstructionCard(
                      value as bool, onCheckboxDeclarationChange);
                },
              ),
              ValueListenableBuilder(
                  valueListenable: _isTermsCheckboxChanged,
                  builder: (context, value, child) {
                    return SignupTermsCard(
                        value as bool, onCheckboxTermsChange);
                  }),
              const SignupInfo(),
              ValueListenableBuilder(
                  valueListenable: _isSignupFormUpdated,
                  builder: (context, value, child) {
                    return SignupButton(
                        nameTextFieldController.text,
                        _taxIdentificationValueChanged.value,
                        _taxClassificationValueChanged.value,
                        businessNameTextFieldController.text,
                        tinTextFieldController.text,
                        payeeCodeTextFieldController.text,
                        addressTextFieldController.text,
                        cityTextFieldController.text,
                        _stateValueChanged.value,
                        zipTextFieldController.text,
                        _isDeclarationCheckboxChanged.value,
                        _isTermsCheckboxChanged.value);
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
