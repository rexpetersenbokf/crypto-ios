import 'package:flutter/material.dart';
import '../../screens/documents_screen.dart';

class DocumentButton extends StatelessWidget {
  const DocumentButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(
              bottom: 16,
            ),
            width: double.infinity,
            height: 1,
            decoration: const BoxDecoration(
              color: Color.fromARGB(
                255,
                189,
                189,
                189,
              ),
            ),
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: () {
              Navigator.of(context).pushNamed(DocumentScreen.routeName);
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: const <Widget>[
                Text(
                  'Documents',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 16,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Icon(
                  Icons.keyboard_arrow_right_outlined,
                  color: Colors.black,
                ),
              ],
            ),
          ),
          Container(
            margin: const EdgeInsets.only(
              top: 16,
            ),
            width: double.infinity,
            height: 1,
            decoration: const BoxDecoration(
              color: Color.fromARGB(
                255,
                189,
                189,
                189,
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(
              top: 32,
            ),
            child: Text(
              '[P] This buy transaction is pending final settlement, which includes delivery of bitcoin to your NYDIG account. This process is typically completed within 1-2 business days.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 11,
                fontWeight: FontWeight.w400,
                color: Color.fromARGB(
                  255,
                  115,
                  115,
                  115,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 16,
              horizontal: 30,
            ),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                text: 'Bitcoin accounts and services provided by NYDIG. See ',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 11,
                  color: Color.fromARGB(255, 115, 115, 115),
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: 'Terms.',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 11,
                      color: Color.fromARGB(255, 115, 115, 115),
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: ' Bitcoin is not insured by the FDIC.',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 11,
                      color: Color.fromARGB(255, 115, 115, 115),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
