import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../config/constant.dart';
import '../config/globals.dart' as global;
import '../providers/account.dart';
import '../providers/order.dart';
import '../screens/quote_screen.dart';

import '../widgets/buy/amount_input.dart';

class BuyBitcoinScreen extends StatefulWidget {
  const BuyBitcoinScreen({Key? key}) : super(key: key);
  static const routeName = '/buyBitcoin';

  @override
  _BuyBitcoinScreen createState() => _BuyBitcoinScreen();
}

class _BuyBitcoinScreen extends State<BuyBitcoinScreen> {
  final List<int> staticBuyOptions = [10, 20, 50, 100, 200, 500];
  final List<String> staticSellOptions = ['Sell 1/2', 'Sell Max'];
  final List<String> buttons = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '.',
    '0',
    ''
  ];

  var _isLoading = false;
  var _isCheckingBalance = false;
  UserAction _userAction = UserAction.buy;

  var _accountBalance = {};
  int _selectedAmountIndex = -1;
  bool _isUserInputInitiated = false;
  String _userInputAmount = '0.00';
  String _currency = '\$';

  Future<void> _callBitcoinAccountBalanceAPI(String accountNumber) async {
    setState(() {
      _isCheckingBalance = true;
    });

    try {
      var response = await Provider.of<Account>(context, listen: false)
          .getBitcoinAccountBalance(accountNumber);
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        var accountInfo = response['crypto'];
        _accountBalance = accountInfo[0];
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isCheckingBalance = false;
    });
  }

  Future<void> _actionPreviewQuoteButton() async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response = await Provider.of<Order>(context, listen: false)
          .getOrderQuote(
              global.userAccount['accountNumber'],
              global.userAccount['customerId'],
              'BTC-USD',
              _userAction == UserAction.buy ? 'BUY' : 'SELL', {
        'currency': _currency == '\$' ? 'USD' : 'BTC',
        'amount': _currency == '\$'
            ? (double.tryParse(_userInputAmount) ?? 0.0).toStringAsFixed(2)
            : (double.tryParse(_userInputAmount) ?? 0.0).toStringAsFixed(8)
      });
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        Navigator.of(context).pushNamed(QuoteScreen.routeName,
            arguments: {'userAction': _userAction, 'orderQuote': response});
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    var _accountNumber = global.userAccount['accountNumber'];
    _callBitcoinAccountBalanceAPI(_accountNumber);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _userAction = ModalRoute.of(context)!.settings.arguments as UserAction;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Future Bank'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AmountInput(_userAction, _userInputAmount, _currency),
            if (((double.tryParse(_userInputAmount) ?? 0.0) <= 100000) &&
                ((double.tryParse(_userInputAmount) ?? 0.0) <=
                    (double.tryParse('${_accountBalance['marketValue']}') ??
                        0.0))) ...[
              const SizedBox(
                height: 56,
              )
            ] else ...[
              Container(
                width: double.infinity,
                height: 56,
                padding: const EdgeInsets.symmetric(
                  horizontal: 70,
                ),
                child: Text(
                  ((double.tryParse(_userInputAmount) ?? 0.0) >
                          (double.tryParse(_userInputAmount) ?? 0.0))
                      ? 'Amount exceeds your bank account balance of \$${_accountBalance['marketValue']}'
                      : 'Amount exceeds the transaction limit of \$100,000.00',
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    color: Color.fromARGB(255, 204, 0, 0),
                    fontFamily: 'Roboto',
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              )
            ],
            GridView.builder(
              padding: const EdgeInsets.all(16),
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              //primary: false,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: _userAction == UserAction.buy ? 3 : 2,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                  childAspectRatio:
                      _userAction == UserAction.buy ? 2 / 0.8 : 2 / 0.5),
              itemCount: _userAction == UserAction.buy
                  ? staticBuyOptions.length
                  : staticSellOptions.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedAmountIndex = index;
                      if (_userAction == UserAction.buy) {
                        _currency = '\$';
                        _userInputAmount = '${staticBuyOptions[index]}';
                      } else {
                        double bitcoin =
                            double.tryParse('${_accountBalance['quantity']}') ??
                                0.0;
                        _currency = '${_accountBalance['currency']}';
                        if (index == 0) {
                          _userInputAmount = (bitcoin / 2.0).toStringAsFixed(6);
                        } else {
                          _userInputAmount = bitcoin.toStringAsFixed(6);
                        }
                      }
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4.0),
                      border: Border.all(
                        color: const Color.fromARGB(255, 4, 209, 184),
                      ),
                      color: _selectedAmountIndex == index
                          ? const Color.fromARGB(255, 4, 209, 184)
                          : null,
                    ),
                    child: Center(
                      child: Text(
                        _userAction == UserAction.buy
                            ? '\$${staticBuyOptions[index]}'
                            : staticSellOptions[index],
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 14,
                          fontWeight: FontWeight.w500,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
            Container(
              height: 40,
              padding: const EdgeInsets.symmetric(horizontal: 8),
              margin: const EdgeInsets.symmetric(
                horizontal: 32,
              ),
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 242, 244, 249),
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Wrap(
                    spacing: 8,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Image.asset(
                        _userAction == UserAction.buy
                            ? 'assets/images/icTimer.png'
                            : 'assets/images/icTimer.png',
                        height: 24,
                        width: 24,
                      ),
                      Text(
                        _userAction == UserAction.buy
                            ? _isCheckingBalance == true
                                ? 'Checking'
                                : 'Balance'
                            : _isCheckingBalance == true
                                ? 'Checking'
                                : 'Bitcoin',
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontFamily: 'Roboto',
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                  if (_userAction == UserAction.buy) ...[
                    _isCheckingBalance == true
                        ? const SizedBox(
                            width: 15,
                            height: 15,
                            child: CircularProgressIndicator())
                        : Text(
                            '\$${_accountBalance['marketValue']}',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: 'Roboto',
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                  ] else ...[
                    _isCheckingBalance == true
                        ? const SizedBox(
                            width: 15,
                            height: 15,
                            child: CircularProgressIndicator())
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    '${_accountBalance['quantity']} ${_accountBalance['currency']}',
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontFamily: 'Roboto',
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  Text(
                                    '\$${_accountBalance['marketValue']}',
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                      fontFamily: 'Roboto',
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.normal,
                                      fontStyle: FontStyle.italic,
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(
                                width: 8,
                              ),
                              Icon(
                                Icons.info_outline,
                                color: Theme.of(context).primaryColor,
                              )
                            ],
                          ),
                  ],
                ],
              ),
            ),
            GridView.builder(
              padding: const EdgeInsets.only(
                left: 16,
                right: 16,
                top: 32,
                bottom: 16,
              ),
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              //primary: false,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  crossAxisSpacing: 8,
                  mainAxisSpacing: 8,
                  childAspectRatio: 2 / 0.8),
              itemCount: buttons.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      _currency = '\$';
                      if (_selectedAmountIndex != -1 &&
                          index != buttons.length - 1) {
                        _selectedAmountIndex = -1;
                        _userInputAmount = '';
                      }

                      if (_isUserInputInitiated == false) {
                        _isUserInputInitiated = true;
                        _userInputAmount = '';
                      }

                      if (index == buttons.length - 1) {
                        _selectedAmountIndex = -1;
                        if (_userInputAmount.isNotEmpty) {
                          _userInputAmount = _userInputAmount.substring(
                              0, _userInputAmount.length - 1);
                        }
                      } else {
                        if ((double.tryParse(_userInputAmount) ?? 0.0) <
                            100000) {
                          _userInputAmount =
                              '$_userInputAmount${buttons[index]}';
                        }
                      }
                    });
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(255, 244, 245, 246),
                      borderRadius: BorderRadius.circular(4.0),
                    ),
                    child: Center(
                      child: index == (buttons.length - 1)
                          ? const Icon(
                              Icons.backspace,
                              color: Color.fromARGB(255, 97, 97, 97),
                              size: 18,
                            )
                          : Text(
                              buttons[index],
                              style: const TextStyle(
                                fontFamily: 'Roboto',
                                fontSize: 25,
                                fontWeight: FontWeight.w400,
                                color: Color.fromARGB(255, 97, 97, 97),
                              ),
                            ),
                    ),
                  ),
                );
              },
            ),
            _isLoading == true
                ? const CircularProgressIndicator()
                : Container(
                    margin: const EdgeInsets.symmetric(
                      horizontal: 16,
                    ),
                    width: double.infinity,
                    height: 52,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          (((double.tryParse(_userInputAmount) ?? 0) >= 0.01) &&
                                  ((double.tryParse(_userInputAmount) ?? 0) <=
                                      100000))
                              ? const Color.fromARGB(255, 0, 112, 228)
                              : const Color.fromARGB(220, 220, 220, 228),
                        ),
                      ),
                      child: const Text(
                        'Preview Quote',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      onPressed:
                          (((double.tryParse(_userInputAmount) ?? 0) >= 0.01) &&
                                  ((double.tryParse(_userInputAmount) ?? 0) <=
                                      100000))
                              ? _actionPreviewQuoteButton
                              : null,
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 32,
                horizontal: 30,
              ),
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: 'Bitcoin accounts and services provided by NYDIG. See ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    color: Color.fromARGB(255, 115, 115, 115),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Terms.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 115, 115, 115),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    TextSpan(
                      text: ' Bitcoin is not insured by the FDIC.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 115, 115, 115),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
