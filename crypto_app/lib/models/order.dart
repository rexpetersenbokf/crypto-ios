class Order {
  String orderQuoteId;
  String customerId;
  String accountNumber;
  String tenantId;
  String symbol;
  String direction;
  Notional notional;
  String price;
  Pay pay;
  Receive receive;
  List<Fee>? fees;
  String expiredAction;
  String createdAt;
  String expiresAt;

  Order(
    this.orderQuoteId,
    this.customerId,
    this.accountNumber,
    this.tenantId,
    this.symbol,
    this.direction,
    this.notional,
    this.price,
    this.pay,
    this.receive,
    this.fees,
    this.expiredAction,
    this.createdAt,
    this.expiresAt,
  );

  factory Order.fromJson(dynamic json) {
    if (json['fees'] != null) {
      var feeObjsJson = json['fees'] as List;
      List<Fee> _fees =
          feeObjsJson.map((tagJson) => Fee.fromJson(tagJson)).toList();

      return Order(
        json['orderQuoteId'] as String,
        json['customerId'] as String,
        json['accountNumber'] as String,
        json['tenantId'] as String,
        json['symbol'] as String,
        json['direction'] as String,
        Notional.fromJson(json['notional']),
        json['price'] as String,
        Pay.fromJson(json['pay']),
        Receive.fromJson(json['receive']),
        _fees,
        json['expiredAction'] as String,
        json['createdAt'] as String,
        json['expiresAt'] as String,
      );
    } else {
      return Order(
        json['orderQuoteId'] as String,
        json['customerId'] as String,
        json['accountNumber'] as String,
        json['tenantId'] as String,
        json['symbol'] as String,
        json['direction'] as String,
        Notional.fromJson(json['notional']),
        json['price'] as String,
        Pay.fromJson(json['pay']),
        Receive.fromJson(json['receive']),
        null,
        json['expiredAction'] as String,
        json['createdAt'] as String,
        json['expiresAt'] as String,
      );
    }
  }

  @override
  String toString() {
    return '{ $orderQuoteId, $customerId, $accountNumber, $tenantId, $symbol, $direction, $notional, $price, $pay, $receive, $fees, $expiredAction, $createdAt, $expiresAt }';
  }
}

class Notional {
  String currency;
  String amount;

  Notional(
    this.currency,
    this.amount,
  );

  factory Notional.fromJson(dynamic json) {
    return Notional(
      json['currency'] as String,
      json['amount'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount }';
  }
}

class Pay {
  String currency;
  String subtotal;
  String total;

  Pay(
    this.currency,
    this.subtotal,
    this.total,
  );

  factory Pay.fromJson(dynamic json) {
    return Pay(
      json['currency'] as String,
      json['subtotal'] as String,
      json['total'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $subtotal, $total }';
  }
}

class Receive {
  String currency;
  String amount;

  Receive(
    this.currency,
    this.amount,
  );

  factory Receive.fromJson(dynamic json) {
    return Receive(
      json['currency'] as String,
      json['amount'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount }';
  }
}

class Fee {
  String currency;
  String amount;
  String rateMethod;
  String rateValue;

  Fee(
    this.currency,
    this.amount,
    this.rateMethod,
    this.rateValue,
  );

  factory Fee.fromJson(dynamic json) {
    return Fee(
      json['currency'] as String,
      json['amount'] as String,
      json['rateMethod'] as String,
      json['rateValue'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount, $rateMethod, $rateValue }';
  }
}
