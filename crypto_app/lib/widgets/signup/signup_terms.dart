import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SignupTermsCard extends StatefulWidget {
  bool isTermsAccepted;
  void Function(bool?)? onCheckboxTermsChange;

  SignupTermsCard(
    this.isTermsAccepted,
    this.onCheckboxTermsChange, {
    Key? key,
  }) : super(key: key);

  @override
  _SignupTermsCard createState() => _SignupTermsCard();
}

class _SignupTermsCard extends State<SignupTermsCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 16,
      ),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CheckboxListTile(
            contentPadding: const EdgeInsets.all(0),
            dense: true,
            title: const Text(
              'By clicking "Sign Up" below, I am requesting to open a bitcoin account with NYDIG. I acknowledge that I have read and agree to:',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 15,
                fontWeight: FontWeight.w500,
                color: Colors.black,
              ),
            ),
            value: widget.isTermsAccepted,
            onChanged: widget.onCheckboxTermsChange,
            controlAffinity: ListTileControlAffinity.leading,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16,
              bottom: 4,
              left: 55,
              right: 16,
            ),
            child: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: '1.  ',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 13,
                  color: Theme.of(context).primaryColor,
                ),
                children: const <TextSpan>[
                  TextSpan(
                    text: 'NYDIG E-sign Consent',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 13,
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16,
              bottom: 4,
              left: 55,
              right: 16,
            ),
            child: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: '2.  ',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 13,
                  color: Theme.of(context).primaryColor,
                ),
                children: const <TextSpan>[
                  TextSpan(
                    text: 'NYDIG Terms and Conditions',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 13,
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16,
              bottom: 4,
              left: 55,
              right: 16,
            ),
            child: RichText(
              textAlign: TextAlign.left,
              text: TextSpan(
                text: '3.  ',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 13,
                  color: Theme.of(context).primaryColor,
                ),
                children: const <TextSpan>[
                  TextSpan(
                    text: 'NYDIG Privacy Policy',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 13,
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
