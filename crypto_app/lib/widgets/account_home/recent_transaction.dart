import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../config/globals.dart' as global;
import '../../config/constant.dart';
import '../../providers/account.dart';

import './transaction_item.dart';
import '../../models/transaction.dart';
import '../../screens/transactions_screen.dart';

class RecentTransactions extends StatefulWidget {
  const RecentTransactions({Key? key}) : super(key: key);

  @override
  _RecentTransactions createState() => _RecentTransactions();
}

class _RecentTransactions extends State<RecentTransactions> {
  var _isLoading = false;
  List<Transaction> _transactions = [];

  Future<void> _callAccountTransactionsAPI(String accountNumber) async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response = await Provider.of<Account>(context, listen: false)
          .getAccountTransactions(accountNumber);
      if (response is List) {
        List<Transaction> objTransactions =
            response.map((tagJson) => Transaction.fromJson(tagJson)).toList();
        _transactions = objTransactions;
      } else if (response is Map<String, dynamic>) {
        var statusCode = response['status'];
        var message = response['title'];
        if (statusCode != null && apiErrorCode.contains(statusCode)) {
          showCustomDialog(context, 'Error', message);
        } else {
          showCustomDialog(context, 'Error', errorMessage);
        }
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    var _accountNumber = global.userAccount['accountNumber'];
    _callAccountTransactionsAPI(_accountNumber);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 32, left: 16, right: 16, bottom: 3),
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                'Recent Transactions',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              _isLoading == true
                  ? const SizedBox(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(),
                    )
                  : TextButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                            TransactionsScreen.routeName,
                            arguments: _transactions);
                      },
                      child: const Text(
                        'See All',
                        style: TextStyle(
                          color: Colors.blue,
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
            ],
          ),
          Column(
            children: _transactions.map((i) {
              return TransactionItem(i);
            }).toList(),
          ),
        ],
      ),
    );
  }
}
