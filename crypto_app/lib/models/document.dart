class Document {
  final String type;
  final String title;
  final bool isLast;

  Document({
    required this.type,
    required this.title,
    required this.isLast,
  });
}
