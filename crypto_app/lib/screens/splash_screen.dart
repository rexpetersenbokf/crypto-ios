import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:amazon_cognito_identity_dart_2/cognito.dart';

import '../config/constant.dart';
import '../../config/globals.dart' as global;

import '../providers/signup.dart';
import '../providers/account.dart';
import '../providers/user_service.dart';

import '../screens/welcome_screen.dart';
import '../screens/dashboard_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreen createState() => _SplashScreen();
}

class _SplashScreen extends State<SplashScreen> {
  final _userService = UserService(CognitoUserPool(userPoolID, clientID));
  bool _isAuthenticated = false;

  Future<UserService> _getUserFromAWS(BuildContext context) async {
    try {
      await _userService.init();
      _isAuthenticated = await _userService.checkAuthenticated();
      if (_isAuthenticated) {
        global.user = await _userService.getCurrentUser();
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, WelcomeScreen.routeName, (_) => false);
      }
      return _userService;
    } on CognitoClientException catch (e) {
      if (e.code == 'NotAuthorizedException') {
        await _userService.signOut();
        Navigator.pushNamedAndRemoveUntil(
            context, WelcomeScreen.routeName, (_) => false);
      }
      rethrow;
    }
  }

  Future<void> _callGetAccountAPI(String accountNumber) async {
    try {
      var response = await Provider.of<Account>(context, listen: false)
          .getAccountByAccountNumber(accountNumber);
      var statusCode = response['status'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        Navigator.pushNamedAndRemoveUntil(
            context, WelcomeScreen.routeName, (_) => false);
      } else {
        global.userAccount = response;
        Navigator.pushNamedAndRemoveUntil(
            context, DashboardScreen.routeName, (_) => false);
      }
    } catch (error) {
      Navigator.pushNamedAndRemoveUntil(
          context, WelcomeScreen.routeName, (_) => false);
    }
  }

  @override
  void initState() {
    super.initState();

    // _getUserFromAWS(context).then((value) {
    //   if (_isAuthenticated) {
    //     Provider.of<Signup>(context, listen: false)
    //         .checkAuthenticationStatus()
    //         .then((accountNumber) {
    //       if (accountNumber != null) {
    //         _callGetAccountAPI(accountNumber);
    //       } else {
    //         Navigator.pushNamedAndRemoveUntil(
    //             context, WelcomeScreen.routeName, (_) => false);
    //       }
    //     });
    //   }
    // });

    Provider.of<Signup>(context, listen: false)
        .checkAuthenticationStatus()
        .then((accountNumber) {
      if (accountNumber != null) {
        _callGetAccountAPI(accountNumber);
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, WelcomeScreen.routeName, (_) => false);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: const Center(
        child: Text(
          'Loading...',
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Roboto',
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }
}
