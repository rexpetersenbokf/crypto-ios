import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../config/globals.dart' as global;
import '../../config/constant.dart';

import '../../screens/dashboard_screen.dart';
import '../../providers/signup.dart';
import '../../providers/user_service.dart';
import '../../models/user.dart';

// ignore: must_be_immutable
class SignupButton extends StatefulWidget {
  String? name;
  String? taxIdentification;
  String? taxClassification;
  String? businessName;
  String? tinNumber;
  String? payeeCode;
  String? address;
  String? city;
  Map<String, String>? state;
  String? zip;
  bool? isDeclarationAccepted;
  bool? isTermsAccepted;

  SignupButton(
      this.name,
      this.taxIdentification,
      this.taxClassification,
      this.businessName,
      this.tinNumber,
      this.payeeCode,
      this.address,
      this.city,
      this.state,
      this.zip,
      this.isDeclarationAccepted,
      this.isTermsAccepted,
      {Key? key})
      : super(key: key);

  @override
  _SignupButtonState createState() => _SignupButtonState();
}

class _SignupButtonState extends State<SignupButton> {
  var _isLoading = false;
  final userService = UserService(CognitoUserPool(userPoolID, clientID));

  bool _checkValidation() {
    if (widget.name == null || widget.name!.isEmpty) {
      showToastPopup(context, 'Please enter your name');
      return false;
    } else if (widget.taxIdentification == null ||
        widget.taxIdentification!.isEmpty) {
      showToastPopup(context, 'Please select your tax classification');
      return false;
    } else if (widget.taxIdentification != 'Individual') {
      if (widget.taxIdentification == 'Limited liability company (LLC)') {
        if (widget.taxClassification == null ||
            widget.taxClassification!.isEmpty) {
          showToastPopup(context, 'Please select LLC tax classification');
          return false;
        } else if (widget.businessName == null ||
            widget.businessName!.isEmpty) {
          showToastPopup(context, 'Please enter business name');
          return false;
        } else if (widget.tinNumber == null || widget.tinNumber!.isEmpty) {
          showToastPopup(context, 'Please enter tax identification number');
          return false;
        } else if (widget.payeeCode == null || widget.payeeCode!.isEmpty) {
          showToastPopup(context, 'Please enter exempt payee code');
          return false;
        } else {
          return _checkValidationAfterTax();
        }
      } else {
        if (widget.businessName == null || widget.businessName!.isEmpty) {
          showToastPopup(context, 'Please enter business name');
          return false;
        } else if (widget.tinNumber == null || widget.tinNumber!.isEmpty) {
          showToastPopup(context, 'Please enter tax identification number');
          return false;
        } else if (widget.payeeCode == null || widget.payeeCode!.isEmpty) {
          showToastPopup(context, 'Please enter exempt payee code');
          return false;
        } else {
          return _checkValidationAfterTax();
        }
      }
    } else if (widget.tinNumber == null || widget.tinNumber!.isEmpty) {
      showToastPopup(context, 'Please enter tax identification number');
      return false;
    } else {
      return _checkValidationAfterTax();
    }
  }

  bool _checkValidationAfterTax() {
    if (widget.address == null || widget.address!.isEmpty) {
      showToastPopup(context, 'Please enter your address');
      return false;
    } else if (widget.city == null || widget.city!.isEmpty) {
      showToastPopup(context, 'Please enter your city');
      return false;
    } else if (widget.state == null || widget.state!.isEmpty) {
      showToastPopup(context, 'Please select your state');
      return false;
    } else if (widget.zip == null || widget.zip!.isEmpty) {
      showToastPopup(context, 'Please enter your zip code');
      return false;
    } else if (widget.isDeclarationAccepted == null ||
        widget.isDeclarationAccepted! == false) {
      showToastPopup(context, 'Please accept the declaration');
      return false;
    } else if (widget.isTermsAccepted == null ||
        widget.isTermsAccepted! == false) {
      showToastPopup(context, 'Please accept the terms');
      return false;
    } else {
      return true;
    }
  }

  Future<void> _callRegisterUserOnCognitoAPI() async {
    setState(() {
      _isLoading = true;
    });

    String message;

    try {
      global.user = await userService.signUp(
          'vikas@yopmail.com', 'Hello@123', widget.name ?? '');
      _callCreateCustomerAPI();
      message = 'User sign up successful!';
    } on CognitoClientException catch (e) {
      if (e.code == 'UsernameExistsException' ||
          e.code == 'InvalidParameterException' ||
          e.code == 'InvalidPasswordException' ||
          e.code == 'ResourceNotFoundException') {
        message = e.message ?? 'Unknown client error occurred';
        showCustomDialog(context, 'Error', message);
      } else {
        message = 'Unknown client error occurred';
        showCustomDialog(context, 'Error', message);
      }
    } catch (e) {
      message = 'Unknown error occurred';
      showCustomDialog(context, 'Error', message);
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _callCreateCustomerAPI() async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response = await Provider.of<Signup>(context, listen: false)
          .createCustomer(
              'PERSONAL', widget.state?['code'] ?? 'AL', false, 'Vikas');
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        var customerId = response['customerId'];
        _callCreateAccountAPI(customerId);
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  Future<void> _callCreateAccountAPI(String customerId) async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response = await Provider.of<Signup>(context, listen: false)
          .createAccount(customerId, '200000096129', 'Vikas');
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        final prefs = await SharedPreferences.getInstance();
        prefs.setString('accountNumber', response['accountNumber']);
        global.userAccount = response;
        Navigator.pushNamedAndRemoveUntil(
            context, DashboardScreen.routeName, (_) => false);
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(
        top: 0,
        bottom: 16,
        left: 16,
        right: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Dated: ${DateFormat('MM/dd/yy').format(DateTime.now())}',
            style: const TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16,
            ),
            textAlign: TextAlign.center,
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16,
              bottom: 4,
            ),
            child: _isLoading == true
                ? const CircularProgressIndicator()
                : SizedBox(
                    width: double.infinity,
                    height: 52,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          const Color.fromARGB(255, 0, 112, 228),
                        ),
                      ),
                      child: const Text(
                        'Sign Up',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      onPressed: () {
                        if (_checkValidation() == true) {
                          //_callCreateCustomerAPI();
                          _callRegisterUserOnCognitoAPI();
                        }
                      },
                    ),
                  ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 0,
              bottom: 32,
            ),
            child: SizedBox(
              width: double.infinity,
              height: 40,
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  primary: Theme.of(context).primaryColor,
                  side: BorderSide(
                      color: Theme.of(context).primaryColor, width: 1.25),
                ),
                child: const Text(
                  'Cancel',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onPressed: () {
                  Navigator.popUntil(context, ModalRoute.withName('/welcome'));
                },
              ),
            ),
          ),
          RichText(
            textAlign: TextAlign.center,
            text: const TextSpan(
              text: 'Bitcoin accounts and services provided by NYDIG. See ',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 11,
                color: Color.fromARGB(255, 115, 115, 115),
              ),
              children: <TextSpan>[
                TextSpan(
                  text: 'Terms.',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    color: Color.fromARGB(255, 115, 115, 115),
                    decoration: TextDecoration.underline,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
