import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/bank_account.dart';
import '../screens/welcome_screen.dart';

import '../widgets/dashboard/name_image.dart';
import '../widgets/dashboard/bank_account_item.dart';
import '../widgets/dashboard/bitcoin_account.dart';

class DashboardScreen extends StatelessWidget {
  DashboardScreen({Key? key}) : super(key: key);
  static const routeName = '/dashbaord';

  final List<BankAccount> bankAccounts = [
    BankAccount(
      accountType: 'Checking',
      accountNumber: '****3081',
      accountBalance: 9749.23,
    ),
    BankAccount(
      accountType: 'Savings',
      accountNumber: '****4650',
      accountBalance: 12301.43,
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Future Bank'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.logout_outlined),
            onPressed: () async {
              final prefs = await SharedPreferences.getInstance();
              prefs.clear();
              final PageRouteBuilder welcomView = PageRouteBuilder(
                pageBuilder: (BuildContext context, _, __) {
                  return WelcomeScreen();
                },
              );

              Navigator.pushAndRemoveUntil(
                  context, welcomView, (Route<dynamic> r) => false);
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const NameImage(),
            const SizedBox(
              height: 43,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Text(
                'Bank Accounts',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: bankAccounts.map((i) {
                  return BankAccountItem(i);
                }).toList(),
              ),
            ),
            const SizedBox(
              height: 50,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Text(
                'Bitcoin Accounts',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                ),
              ),
            ),
            const BitcoinAccount(),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 32,
                horizontal: 30,
              ),
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: 'Bitcoin accounts and services provided by NYDIG. See ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    color: Color.fromARGB(255, 115, 115, 115),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Terms.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 115, 115, 115),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    TextSpan(
                      text:
                          ' Bitcoin is not insured by the FDIC. Bitcoin is not a security and NYDIG is not an SEC-registered exchange. Bitcoin balance in your NYDIG account are not insured by the Securities Investor Protection Corporation (SIPC).',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 115, 115, 115),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
