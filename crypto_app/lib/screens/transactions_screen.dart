import 'package:flutter/material.dart';
import '../models/transaction.dart';
import '../widgets/account_home/transaction_item.dart';

class TransactionsScreen extends StatelessWidget {
  const TransactionsScreen({Key? key}) : super(key: key);
  static const routeName = '/allTransaction';

  @override
  Widget build(BuildContext context) {
    final _allTransactions =
        ModalRoute.of(context)!.settings.arguments as List<Transaction>;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Future Bank'),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(16),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              width: double.infinity,
              child: Text(
                'All Transaction',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 28,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            const SizedBox(height: 40),
            Expanded(
              child: ListView.builder(
                itemCount: _allTransactions.length,
                itemBuilder: (BuildContext context, int index) {
                  return TransactionItem(_allTransactions[index]);
                },
              ),
            ),
            // Container(
            //   width: 170,
            //   height: 40,
            //   margin: const EdgeInsets.only(top: 20),
            //   child: OutlinedButton(
            //     style: OutlinedButton.styleFrom(
            //       primary: Theme.of(context).primaryColor,
            //       side: BorderSide(
            //           color: Theme.of(context).primaryColor, width: 1.25),
            //     ),
            //     child: Row(
            //       mainAxisAlignment: MainAxisAlignment.center,
            //       children: const [
            //         Text(
            //           'Load More',
            //           style: TextStyle(
            //             fontFamily: 'Roboto',
            //             fontSize: 15,
            //             fontWeight: FontWeight.w700,
            //           ),
            //         ),
            //         Icon(
            //           Icons.arrow_drop_down,
            //           size: 30,
            //         ),
            //       ],
            //     ),
            //     onPressed: () {},
            //   ),
            // ),
            const SizedBox(height: 25),
            const SafeArea(
              bottom: true,
              child: Text(
                '[P] This buy transaction is pending final settlement, which includes delivery of bitcoin to your NYDIG account. This process is typically completed within 1-2 business days.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 11,
                  fontWeight: FontWeight.w400,
                  color: Color.fromARGB(
                    255,
                    115,
                    115,
                    115,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
