import 'package:flutter/material.dart';

// ignore: must_be_immutable
class SignupInstructionCard extends StatefulWidget {
  bool isDeclarationAccepted;
  void Function(bool?)? onCheckboxDeclarationChange;

  SignupInstructionCard(
    this.isDeclarationAccepted,
    this.onCheckboxDeclarationChange, {
    Key? key,
  }) : super(key: key);

  @override
  _SignupInstructionCard createState() => _SignupInstructionCard();
}

class _SignupInstructionCard extends State<SignupInstructionCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 240, 240, 240),
      ),
      padding: const EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 16,
      ),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CheckboxListTile(
            contentPadding: const EdgeInsets.all(0),
            dense: true,
            title: const Text(
              'I certify under penalties of perjury that:',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 15,
                fontWeight: FontWeight.w500,
                color: Colors.black,
              ),
            ),
            value: widget.isDeclarationAccepted,
            onChanged: widget.onCheckboxDeclarationChange,
            controlAffinity: ListTileControlAffinity.leading,
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: const Color.fromARGB(255, 189, 189, 189),
                width: 0.5,
              ),
              borderRadius: BorderRadius.circular(8),
            ),
            child: const Padding(
              padding: EdgeInsets.symmetric(
                vertical: 20,
                horizontal: 16,
              ),
              child: Text(
                '1. The number shown above is my correct taxpayer identification number; and\n\n2. I am not subject to backup withholding because: (a) I am exempt from backup withholding, or (b) I have not been notified by the Internal Revenue Service (IRS) that I am subject to backup withholding as a result of a faliure to report all interest or dividends, or (c) the IRS has notified me that I am no longer subject to backup withholding\n\n3. I am a U.S. citizen or other U.S. person\n\n4. The FATCA code(s) entered on this form (if any) indicating that I am exempt from FATCA reporting is correct',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 12,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 24,
              bottom: 16,
              left: 32,
              right: 32,
            ),
            child: RichText(
              textAlign: TextAlign.center,
              text: const TextSpan(
                text:
                    'IF YOU CANNOT AGREE TO EACH OF THE CERTIFICATIONS ABOVE, YOU MAY NOT PROCEED. PLEASE CLICK ',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 12,
                  color: Colors.black,
                ),
                children: <TextSpan>[
                  TextSpan(
                    text: 'CANCEL',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: '\n\nSEE ',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      color: Colors.black,
                    ),
                  ),
                  TextSpan(
                    text: 'INSTRUCTIONS',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      color: Colors.blue,
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(
                    text: ' FOR CERTIFICATION.',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
