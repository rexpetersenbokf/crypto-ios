import 'package:flutter/material.dart';
import '../models/welcome.dart';
import '../widgets/welcome_item.dart';
import 'signup_screen.dart';
import 'login_screen.dart';

class WelcomeScreen extends StatelessWidget {
  static const routeName = '/welcome';
  WelcomeScreen({Key? key}) : super(key: key);

  final List<Welcome> items = [
    Welcome(
      icon: 'assets/images/icTimer.png',
      title: 'Get set up in minutes',
    ),
    Welcome(
      icon: 'assets/images/icTimer.png',
      title: 'Buy bitcoin using your bank account',
    ),
    Welcome(
      icon: 'assets/images/icTimer.png',
      title: 'Manage your bitcoin in the app',
    ),
    Welcome(
      icon: 'assets/images/icTimer.png',
      title: 'Sell your bitcoin for USD at any time',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Future Bank'),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 24,
                  horizontal: 16,
                ),
                child: const Text(
                  'Sign up today for the Bitcoin Trading Platform!',
                  style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'Roboto',
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ),
              Column(
                children: items.map((i) {
                  return WelcomeItem(i);
                }).toList(),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 40,
                  horizontal: 16,
                ),
                child: SizedBox(
                  width: double.infinity,
                  height: 56,
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(
                        const Color.fromARGB(255, 0, 112, 228),
                      ),
                    ),
                    child: const Text(
                      'Sign Up',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 18,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushNamed(SignupScreen.routeName);
                    },
                  ),
                ),
              ),
              const Text(
                'Already have an account?',
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: Color.fromARGB(255, 115, 115, 115),
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              SizedBox(
                width: 200,
                height: 50,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Theme.of(context).primaryColor,
                    ),
                  ),
                  child: const Text(
                    'Login Now',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed(LoginScreen.routeName);
                  },
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(
                  left: 16,
                  right: 16,
                  top: 56,
                  bottom: 30,
                ),
                child: Text(
                  'This website is provided for informational purposes only. None of the material on NYDIG.com is intended to be, nor does it constitute, a solicitation, recommendation or offer to buy or sell any security, financial product or instrument. Investors should conduct their own analysis and consult with professional advisors prior to making any investment decisions. Bitcoin investments have historically been highly volatile and are for investors with a high risk tolerance. Investors in bitcoin could lose the entire value of their investment. All statistical information is as of September 30, 2021.\n\nOnly certain NYDIG legal entities or businesses are regulated. New York Digital Investment Group LLC is not overseen by any regulator. Please see the NYDIG disclosures or terms and conditions for the particular service or transaction for more details. NYDIG Execution LLC (NMLS ID: 1781446) is licensed to engage in Virtual Currency Business Activity by the New York State Department of Financial Services. NYDIG Trust Company LLC (NMLS ID: 1985471) is chartered as a limited purpose trust company by the New York State Department of Financial Services.\n\nFull-Service Brokerage refers to bitcoin trading services provided by NYDIG Execution LLC and NYDIG Trust Company LLC; neither entity is registered as a broker-dealer with the SEC or FCM with the NFA or CFTC. NYDIG Trust Company LLC does business under registered dba names in various states as disclosed here. NYDIG, NYDIG, and NEW YORK DIGITAL INVESTMENT GROUP are registered trademarks of NYDIG. All rights reserved.',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    fontWeight: FontWeight.w400,
                    color: Color.fromARGB(255, 115, 115, 115),
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
