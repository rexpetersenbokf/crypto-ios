import 'package:flutter/material.dart';

import '../screens/dashboard_screen.dart';

import '../widgets/account_home/bitcoin_value.dart';
import '../widgets/account_home/balance.dart';
import '../widgets/account_home/buy_sell_button.dart';
import '../widgets/account_home/recent_transaction.dart';
import '../widgets/account_home/document_button.dart';

class AccountHomeScreen extends StatelessWidget {
  const AccountHomeScreen({Key? key}) : super(key: key);
  static const routeName = '/accountHome';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        elevation: 0,
        title: const Text('Future Bank'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.of(context).popUntil((route) {
              return route.settings.name == DashboardScreen.routeName;
            });
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: const <Widget>[
            BitcoinValue(),
            Balance(),
            BuySellButton(),
            RecentTransactions(),
            DocumentButton(),
          ],
        ),
      ),
    );
  }
}
