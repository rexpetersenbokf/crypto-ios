import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import '../config/api_endpoints.dart' as api;
import '../config/server_config.dart';

class Order with ChangeNotifier {
  Future<Map<String, dynamic>> getOrderQuote(
    String accountNumber,
    String customerId,
    String symbol,
    String direction,
    Map<String, dynamic> notional,
  ) async {
    final apiUrl = Uri.parse('${Constants.serverURL}${api.kGetOrderQuote}');
    try {
      final response = await http.post(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
        body: json.encode(
          {
            'accountNumber': accountNumber,
            'customerId': customerId,
            'symbol': symbol,
            'direction': direction,
            'expiredAction': 'REFRESH',
            'notional': notional,
          },
        ),
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      rethrow;
    }
  }

  Future<Map<String, dynamic>> confirmOrderQuote(
    String accountNumber,
    String customerId,
    String orderQuoteId,
  ) async {
    final apiUrl = Uri.parse('${Constants.serverURL}${api.kConfirmOrderQuote}');
    try {
      final response = await http.post(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
        body: json.encode(
          {
            'accountNumber': accountNumber,
            'customerId': customerId,
            'ref': 'Vikas',
            'orderQuoteId': orderQuoteId,
          },
        ),
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      rethrow;
    }
  }
}
