import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../config/globals.dart' as global;
import '../../config/constant.dart';
import '../../providers/account.dart';

class Balance extends StatefulWidget {
  const Balance({Key? key}) : super(key: key);

  @override
  _Balance createState() => _Balance();
}

class _Balance extends State<Balance> {
  var _isLoading = false;
  var _accountBalance = {};

  Future<void> _callBitcoinAccountBalanceAPI(String accountNumber) async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response = await Provider.of<Account>(context, listen: false)
          .getBitcoinAccountBalance(accountNumber);
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        var accountInfo = response['crypto'];
        _accountBalance = accountInfo[0];
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    var _accountNumber = global.userAccount['accountNumber'];
    _callBitcoinAccountBalanceAPI(_accountNumber);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'BITCOIN ACCOUNT VALUE',
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                  fontFamily: 'Roboto',
                  fontSize: 11.0,
                  fontWeight: FontWeight.normal,
                ),
              ),
              IconButton(
                icon: const Icon(
                  Icons.info_outline,
                  size: 22,
                ),
                color: Theme.of(context).primaryColor,
                onPressed: () {
                  showCustomDialog(context, 'Account Balance',
                      'If you have a business name, trade name, DBA name or disregarded entity name, you may enter it here.');
                },
              ),
            ],
          ),
          _isLoading == true
              ? const CircularProgressIndicator()
              : Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 26,
                        bottom: 16,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '\$',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: 'Roboto',
                              fontSize: 28.0,
                              fontWeight: FontWeight.w300,
                            ),
                          ),
                          const SizedBox(
                            width: 8,
                          ),
                          Text(
                            '${_accountBalance['marketValue']}',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: 'Roboto',
                              fontSize: 64.0,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width / 1.5,
                      height: 1,
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 4, 209, 184),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 24),
                      child: Text(
                        '${_accountBalance['quantity']} ${_accountBalance['currency']}',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontFamily: 'Roboto',
                          fontSize: 15.0,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                )
        ],
      ),
    );
  }
}
