class BankAccount {
  final String accountType;
  final String accountNumber;
  final double accountBalance;

  BankAccount({
    required this.accountType,
    required this.accountNumber,
    required this.accountBalance,
  });
}
