import 'package:flutter/material.dart';

import '../models/document.dart';
import '../widgets/account_home/document_item.dart';

class DocumentScreen extends StatelessWidget {
  const DocumentScreen({Key? key}) : super(key: key);
  static const routeName = '/allDocuments';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Future Bank'),
      ),
      body: Container(
        width: double.infinity,
        padding: const EdgeInsets.all(16),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              width: double.infinity,
              child: Text(
                'Documents',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 28,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            const SizedBox(height: 32),
            Expanded(
              child: ListView.builder(
                  itemCount: 6,
                  itemBuilder: (BuildContext context, int index) {
                    return DocumentItem(
                      Document(
                          type: 'Tax Document',
                          title: 'NYDIG Bitcoin Account 1099B - 2020',
                          isLast: index == 5 ? true : false),
                    );
                  }),
            ),
            const SizedBox(height: 25),
            SafeArea(
              bottom: true,
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: 'Bitcoin accounts and services provided by NYDIG. See ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    color: Color.fromARGB(255, 115, 115, 115),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Terms.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 115, 115, 115),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
