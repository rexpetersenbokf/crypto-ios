import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../config/constant.dart';
import '../../models/transaction.dart';

import '../../screens/transaction_receipt_screen.dart';

class TransactionItem extends StatelessWidget {
  final Transaction transaction;
  // ignore: use_key_in_widget_constructors
  const TransactionItem(this.transaction);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed(TransactionReceiptScreen.routeName,
            arguments: UserAction.buy);
      },
      child: Row(
        children: <Widget>[
          Container(
            height: 72,
            width: 24,
            padding: const EdgeInsets.only(
              top: 10,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'assets/images/icTimer.png',
                  height: 20,
                  width: 20,
                ),
                Expanded(
                  child: Container(
                    margin: const EdgeInsets.only(
                      top: 5,
                    ),
                    width: 1,
                    height: 30,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(
                left: 10,
                top: 8,
              ),
              height: 72,
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        '${transaction.transactionType} (${transaction.trade.direction})',
                        style: const TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Text(
                        transaction.currency == 'USD'
                            ? '\$${double.parse(transaction.amount).toStringAsFixed(2)}'
                            : '${double.parse(transaction.amount).toStringAsFixed(8)} ${transaction.currency}',
                        style: const TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        DateFormat("MM/dd/yyyy hh:mma")
                            .format(DateTime.parse(transaction.tradeDate)),
                        style: const TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      Text(
                        transaction.currency == 'USD'
                            ? '${(double.parse(transaction.amount) / double.parse(transaction.price)).toStringAsFixed(8)} BTC'
                            : '\$${(double.parse(transaction.amount) * double.parse(transaction.price)).toStringAsFixed(2)}',
                        style: const TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 72,
            width: 24,
            margin: const EdgeInsets.only(
              left: 10,
            ),
            child: const Center(
              child: Icon(
                Icons.keyboard_arrow_right_outlined,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
