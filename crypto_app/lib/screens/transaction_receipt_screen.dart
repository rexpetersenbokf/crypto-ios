import 'package:flutter/material.dart';
import '../config/constant.dart';
import '../models/quote_model.dart';
import '../widgets/buy/quote_item.dart';

class TransactionReceiptScreen extends StatelessWidget {
  TransactionReceiptScreen({Key? key}) : super(key: key);
  static const routeName = '/transactionReceipt';
  static const textColor = Color.fromARGB(255, 66, 66, 66);

  final List<QuoteModel> receiptItems = [
    QuoteModel(
      title: 'Type',
      info: 'Bitcoin purchase',
      subinfo: '',
    ),
    QuoteModel(
      title: 'Date & time',
      info: '02/02/2021 11:45pm ET',
      subinfo: '',
    ),
    QuoteModel(
      title: 'Price per bitcoin',
      info: '\$56,842.31',
      subinfo: '',
    ),
    QuoteModel(
      title: 'Bitcoin amount',
      info: '0.00439813 BTC',
      subinfo: '',
    ),
    QuoteModel(
      title: 'Net cost',
      info: '\$98.50',
      subinfo: '',
    ),
    QuoteModel(
      title: 'Transaction fee',
      info: '1.5% (\$1.50)',
      subinfo: '',
    ),
    QuoteModel(
      title: 'Gross cost',
      info: '\$100.00',
      subinfo: '',
    ),
    QuoteModel(
      title: 'Bank account',
      info: 'Checking ***3081',
      subinfo: '',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    final userAction = ModalRoute.of(context)!.settings.arguments as UserAction;

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Future Bank'),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.only(top: 24, left: 16, right: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(
                width: double.infinity,
                child: Text(
                  'Transaction Receipt',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 28,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              const SizedBox(height: 40),
              const Text(
                'Receipt ID:',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: textColor,
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const Text(
                '01-f7cf88-ppts3r-8gszk7-trhcfd',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 16),
              const Text(
                'Kira Simpson',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: textColor,
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const Text(
                '123 Brontosaurus Blvd',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: textColor,
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const Text(
                'Dinoaur CO 81610-8691',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: textColor,
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const Text(
                '(813) 867-5309',
                textAlign: TextAlign.left,
                style: TextStyle(
                  color: textColor,
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w400,
                ),
              ),
              const SizedBox(height: 16),
              RichText(
                textAlign: TextAlign.left,
                text: const TextSpan(
                  text:
                      'If you have any question or concerns, please contact us by phone at 1-800-123-4567 or via our online support at ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 14,
                    color: textColor,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'https://www.futurebank.com/support',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 14,
                        color: Color.fromARGB(255, 4, 209, 184),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 24),
              Column(
                children: userAction == UserAction.buy
                    ? receiptItems.map((i) {
                        return QuoteItem(i);
                      }).toList()
                    : receiptItems.map((i) {
                        return QuoteItem(i);
                      }).toList(),
              ),
              const SizedBox(height: 40),
              const SafeArea(
                bottom: true,
                child: Text(
                  'This receipt is provided by NYDIG Execution LLC. NYDIG is located at 510 Madison Avenue, 21st Floor, New York, NY 10022. NYDIG will be responsible for non-delivery or delayed delivery in limited circumstances (please see Terms and Conditions). All transactions are final and non-refundable.\n\nConsumer Fraud Warning: Consumers are warned to be aware of fraud and potential scams. If you think you have been the victim of fraud, please contact us at the telephone number or email listed above to report fraud or suspected fraud.',
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    fontWeight: FontWeight.w400,
                    color: Color.fromARGB(
                      255,
                      115,
                      115,
                      115,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
