import 'package:flutter/material.dart';
import '../../config/constant.dart';

class AmountInput extends StatelessWidget {
  final UserAction action;
  final String amount;
  final String currency;
  // ignore: use_key_in_widget_constructors
  const AmountInput(this.action, this.amount, this.currency);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 24,
        horizontal: 16,
      ),
      child: Column(
        children: <Widget>[
          Text(
            action == UserAction.buy ? 'Buy Bitcoin' : 'Sell Bitcoin',
            style: const TextStyle(
              color: Colors.black,
              fontFamily: 'Roboto',
              fontSize: 28.0,
              fontWeight: FontWeight.w500,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              top: 16,
              bottom: 16,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  currency,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontFamily: 'Roboto',
                    fontSize: 28.0,
                    fontWeight: FontWeight.w300,
                  ),
                ),
                const SizedBox(
                  width: 8,
                ),
                amount.isNotEmpty
                    ? Flexible(
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            amount,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            softWrap: false,
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontFamily: 'Roboto',
                              fontSize: 60.0,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                      )
                    : Text(
                        amount,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: TextStyle(
                          color: Theme.of(context).primaryColor,
                          fontFamily: 'Roboto',
                          fontSize: 60.0,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width / 1.5,
            height: 1,
            decoration: const BoxDecoration(
              color: Color.fromARGB(255, 4, 209, 184),
            ),
          ),
        ],
      ),
    );
  }
}
