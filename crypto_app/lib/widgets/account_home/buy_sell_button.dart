import 'package:flutter/material.dart';

import '../../config/constant.dart';
import '../../screens/buy_bitcoin_screen.dart';

class BuySellButton extends StatelessWidget {
  const BuySellButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 38,
        left: 16,
        right: 16,
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: Container(
              padding: const EdgeInsets.only(
                top: 0,
                right: 2,
              ),
              child: SizedBox(
                width: double.infinity,
                height: 52,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      const Color.fromARGB(255, 0, 112, 228),
                    ),
                  ),
                  child: const Text(
                    'Buy Bitcoin',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 15,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed(BuyBitcoinScreen.routeName,
                        arguments: UserAction.buy);
                  },
                ),
              ),
            ),
          ),
          Expanded(
            flex: 5,
            child: Container(
              padding: const EdgeInsets.only(
                top: 0,
                left: 2,
              ),
              child: SizedBox(
                width: double.infinity,
                height: 52,
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(
                      Theme.of(context).primaryColor,
                    ),
                  ),
                  child: const Text(
                    'Sell Bitcoin',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 15,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed(BuyBitcoinScreen.routeName,
                        arguments: UserAction.sell);
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
