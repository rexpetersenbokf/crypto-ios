// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

class SignupNameCard extends StatefulWidget {
  TextEditingController nameTextFieldController;
  SignupNameCard(this.nameTextFieldController, {Key? key}) : super(key: key);

  @override
  _SignupNameCard createState() => _SignupNameCard();
}

class _SignupNameCard extends State<SignupNameCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 16,
        horizontal: 16,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Name',
            style: TextStyle(
              fontFamily: 'Roboto',
              fontSize: 16,
              fontWeight: FontWeight.w700,
              color: Theme.of(context).primaryColor,
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(
              top: 2,
              bottom: 8,
            ),
            child: Text(
              'As shown on your income tax return. Please note: if your entity is a disregarded entity for tax purposes, please put the name of the regarded entity/owner here.',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 12,
                fontWeight: FontWeight.w400,
                color: Color.fromARGB(255, 92, 92, 92),
              ),
            ),
          ),
          TextField(
            decoration: const InputDecoration(
              floatingLabelBehavior: FloatingLabelBehavior.never,
              isDense: true,
              hintText: 'Enter your name',
              labelText: 'Name',
              border: OutlineInputBorder(),
            ),
            controller: widget.nameTextFieldController,
            keyboardType: TextInputType.name,
            autocorrect: false,
            textCapitalization: TextCapitalization.words,
          ),
        ],
      ),
    );
  }
}
