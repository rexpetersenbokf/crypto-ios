import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../config/constant.dart';
import '../config/globals.dart' as global;

import '../providers/order.dart';

import '../models/order.dart' as om;
import '../models/quote_model.dart';

import '../widgets/buy/amount_input.dart';
import '../widgets/buy/quote_item.dart';

import '../screens/buy_confirm_state.dart';

class QuoteScreen extends StatefulWidget {
  final Map<String, dynamic> _arguments;
  QuoteScreen(this._arguments, {Key? key}) : super(key: key);

  @override
  _QuoteScreen createState() => _QuoteScreen();

  static const routeName = '/quote';
  final List<QuoteModel> buyQuotes = [];
  final List<QuoteModel> sellQuotes = [];

  void _loadBuyInfo(om.Order orderQuote) {
    var direction = orderQuote.direction;
    if (direction == 'BUY') {
      var totalCost = (double.tryParse(orderQuote.pay.total) ?? 0.0) +
          (double.tryParse(orderQuote.fees?[0].amount ?? '0.0') ?? 0.0);

      var quote1 = QuoteModel(
          title: 'Buying',
          info: '${orderQuote.receive.amount} ${orderQuote.receive.currency}',
          subinfo: '\$${orderQuote.pay.total}');
      var quote2 =
          QuoteModel(title: 'Price per bitcoin', info: '\$${orderQuote.price}');
      var quote3 = QuoteModel(
          title: 'Transaction Fee', info: '\$${orderQuote.fees?[0].amount}');
      var quote4 = QuoteModel(
          title: 'Bank Account', info: global.userAccount['accountNumber']);
      var quote5 =
          QuoteModel(title: 'Cost', info: '\$${totalCost.toStringAsFixed(2)}');

      buyQuotes.add(quote1);
      buyQuotes.add(quote2);
      buyQuotes.add(quote3);
      buyQuotes.add(quote4);
      buyQuotes.add(quote5);
    } else {
      var totalCost = (double.tryParse(orderQuote.receive.amount) ?? 0.0);

      var quote1 = QuoteModel(
          title: 'Selling',
          info: orderQuote.notional.currency == 'USD'
              ? ('\$${orderQuote.notional.amount}')
              : ('${orderQuote.notional.amount} ${orderQuote.notional.currency}'),
          subinfo: orderQuote.notional.currency == 'USD'
              ? ('${orderQuote.pay.total} ${orderQuote.pay.currency}')
              : ('\$${orderQuote.receive.amount}'));
      var quote2 =
          QuoteModel(title: 'Price per bitcoin', info: '\$${orderQuote.price}');
      var quote3 = QuoteModel(
          title: 'Transaction Fee', info: '\$${orderQuote.fees?[0].amount}');
      var quote4 = QuoteModel(
          title: 'Bank Account', info: global.userAccount['accountNumber']);
      var quote5 = QuoteModel(
          title: 'Net Sale Proceeds',
          info: '\$${totalCost.toStringAsFixed(2)}');

      sellQuotes.add(quote1);
      sellQuotes.add(quote2);
      sellQuotes.add(quote3);
      sellQuotes.add(quote4);
      sellQuotes.add(quote5);
    }
  }
}

class _QuoteScreen extends State<QuoteScreen> {
  var _isLoading = false;
  UserAction _userAction = UserAction.buy;
  late om.Order _orderQuote;

  Future<void> _actionConfirmQuoteButton() async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response =
          await Provider.of<Order>(context, listen: false).confirmOrderQuote(
        global.userAccount['accountNumber'],
        global.userAccount['customerId'],
        _orderQuote.orderQuoteId,
      );
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        Navigator.of(context).pushReplacementNamed(
          BuyConfirmState.routeName,
          arguments: {
            'state': _userAction == UserAction.buy
                ? BuyState.success
                : BuyState.success,
            'action': _userAction,
            'quote': response
          },
        );
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    _userAction = widget._arguments['userAction'] as UserAction;
    _orderQuote = om.Order.fromJson(
        widget._arguments['orderQuote'] as Map<String, dynamic>);

    widget._loadBuyInfo(_orderQuote);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Future Bank')),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            AmountInput(
                _userAction,
                _orderQuote.notional.amount,
                _orderQuote.notional.currency == 'USD'
                    ? '\$'
                    : _orderQuote.notional.currency),
            Container(
              padding: const EdgeInsets.only(
                left: 16,
                right: 16,
                bottom: 16,
              ),
              width: double.infinity,
              child: const Text(
                'Quote Details',
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 16,
              ),
              child: Column(
                children: _userAction == UserAction.buy
                    ? widget.buyQuotes.map((i) {
                        return QuoteItem(i);
                      }).toList()
                    : widget.sellQuotes.map((i) {
                        return QuoteItem(i);
                      }).toList(),
              ),
            ),
            _isLoading == true
                ? const CircularProgressIndicator()
                : Container(
                    margin: const EdgeInsets.only(
                      left: 16,
                      right: 16,
                      top: 13,
                    ),
                    width: double.infinity,
                    height: 52,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          const Color.fromARGB(255, 0, 112, 228),
                        ),
                      ),
                      child: Text(
                        _userAction == UserAction.buy
                            ? 'Confirm Buy'
                            : 'Confirm Sell',
                        style: const TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 15,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      onPressed: _actionConfirmQuoteButton,
                    ),
                  ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: 32,
                horizontal: 30,
              ),
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: 'Bitcoin accounts and services provided by NYDIG. See ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    color: Color.fromARGB(255, 115, 115, 115),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Terms.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 115, 115, 115),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    TextSpan(
                      text: ' You cannot cancel once you click to confirm.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 115, 115, 115),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
