import 'package:flutter/material.dart';
import 'package:amazon_cognito_identity_dart_2/cognito.dart';

enum Error { ineligible, unavailable, maintenance }
enum BuyState { expired, success }
enum UserAction { buy, sell }

const String userPoolID = 'us-east-1_qaNgmO3ea';
const String clientID = '1p2h6p65bhd1p5v47vugloc37f';
CognitoUserPool userPool = CognitoUserPool(userPoolID, clientID);

const String errorMessage = 'An error occured. Please try again later.';

const List<String> identificationDropdown = [
  'Individual',
  'Sole proprietor',
  'Single member LLC',
  'Limited liability company (LLC)',
  'C corporation',
  'S corporation',
  'Partnership'
];

const List<String> taxClasificationDropdown = [
  'C corporation',
  'S corporation',
  'Partnership'
];

const List<Map<String, String>> stateDropdown = [
  {'name': 'Alabama', 'code': 'AL'},
  {'name': 'Alaska', 'code': 'AK'},
  {'name': 'Arkansas', 'code': 'AR'},
  {'name': 'Arizona', 'code': 'AZ'},
  {'name': 'California', 'code': 'CA'},
  {'name': 'Colorado', 'code': 'CO'},
];

const List<String> apiErrorCode = [
  '400',
  '404',
  '409',
  '422',
];

void showCustomDialog(BuildContext context, String title, String message) {
  showDialog(
    context: context,
    builder: (ctx) => AlertDialog(
      title: Text(title),
      content: Text(message),
      actions: <Widget>[
        SizedBox(
          width: double.infinity,
          height: 40,
          child: OutlinedButton(
            style: OutlinedButton.styleFrom(
              primary: Theme.of(context).primaryColor,
              side: BorderSide(
                  color: Theme.of(context).primaryColor, width: 1.25),
            ),
            child: const Text(
              'Close',
              style: TextStyle(
                fontFamily: 'Roboto',
                fontSize: 13,
                fontWeight: FontWeight.w500,
              ),
            ),
            onPressed: () {
              Navigator.of(ctx).pop();
            },
          ),
        ),
      ],
    ),
  );
}

void showToastPopup(BuildContext context, String message) {
  final snackBar = SnackBar(
    content: Text(message),
  );

  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
