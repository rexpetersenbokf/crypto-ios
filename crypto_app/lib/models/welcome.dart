class Welcome {
  final String icon;
  final String title;

  Welcome({
    required this.icon,
    required this.title,
  });
}
