import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;

import '../config/api_endpoints.dart' as api;
import '../config/server_config.dart';

class Account with ChangeNotifier {
  Future<Map<String, dynamic>> getAccountByAccountNumber(
    String accountNumber,
  ) async {
    final apiUrl = Uri.parse(
        '${Constants.serverURL}${api.kGetAccountByAccountNumber}/$accountNumber');
    try {
      final response = await http.get(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      //print(error.toString());
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getBitcoinAccountBalance(
    String accountNumber,
  ) async {
    final apiUrl = Uri.parse(
        '${Constants.serverURL}${api.kGetBitcoinAccountBalance}/$accountNumber');
    try {
      final response = await http.get(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      //print(error.toString());
      rethrow;
    }
  }

  Future<Map<String, dynamic>> getBitcoinPrice(
    String symbol,
  ) async {
    final apiUrl = Uri.parse(
        '${Constants.serverURL}${api.kGetBitcoinPrice}?symbol=$symbol');
    try {
      final response = await http.get(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      //print(error.toString());
      rethrow;
    }
  }

  Future<dynamic> getAccountTransactions(
    String accountNumber,
  ) async {
    final apiUrl = Uri.parse(
        '${Constants.serverURL}${api.kGetAccountTransactions}/$accountNumber');
    try {
      final response = await http.get(
        apiUrl,
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'X-NYDIG-PARTNER-ID': 'pa-01f7vc1tyrnrn7h3dzqrrxp1jx',
          'X-NYDIG-TENANT-ID': 'tn-01fcy290s675x46m6a6tn415e0',
        },
      );

      final responseData = json.decode(response.body);
      notifyListeners();
      return responseData;
    } catch (error) {
      //print(error.toString());
      rethrow;
    }
  }
}
