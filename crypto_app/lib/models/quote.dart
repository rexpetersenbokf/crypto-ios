class Quote {
  String orderId;
  String customerId;
  String accountNumber;
  String tenantId;
  String status;
  String symbol;
  String direction;
  Notional notional;
  String receipt;
  Trade trade;
  String createdActor;
  String createdAt;
  String createdBy;
  String updatedActor;
  String updatedAt;
  String updatedBy;

  Quote(
    this.orderId,
    this.customerId,
    this.accountNumber,
    this.tenantId,
    this.status,
    this.symbol,
    this.direction,
    this.notional,
    this.receipt,
    this.trade,
    this.createdActor,
    this.createdAt,
    this.createdBy,
    this.updatedActor,
    this.updatedAt,
    this.updatedBy,
  );

  factory Quote.fromJson(dynamic json) {
    return Quote(
      json['orderId'] as String,
      json['customerId'] as String,
      json['accountNumber'] as String,
      json['tenantId'] as String,
      json['status'] as String,
      json['symbol'] as String,
      json['direction'] as String,
      Notional.fromJson(json['notional']),
      json['receipt'] as String,
      Trade.fromJson(json['trade']),
      json['createdActor'] as String,
      json['createdAt'] as String,
      json['createdBy'] as String,
      json['updatedActor'] as String,
      json['updatedAt'] as String,
      json['updatedBy'] as String,
    );
  }

  @override
  String toString() {
    return '{ $orderId, $customerId, $accountNumber, $tenantId, $status, $symbol, $direction, $notional, $receipt, $trade, $createdActor, $createdAt, $createdBy, $updatedActor, $updatedAt, $updatedBy }';
  }
}

class Notional {
  String currency;
  String amount;

  Notional(
    this.currency,
    this.amount,
  );

  factory Notional.fromJson(dynamic json) {
    return Notional(
      json['currency'] as String,
      json['amount'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount }';
  }
}

class Trade {
  String price;
  Pay pay;
  Receive receive;
  List<Fee>? fees;
  String tradeDate;
  String executedAt;

  Trade(
    this.price,
    this.pay,
    this.receive,
    this.fees,
    this.tradeDate,
    this.executedAt,
  );

  factory Trade.fromJson(dynamic json) {
    if (json['fees'] != null) {
      var feeObjsJson = json['fees'] as List;
      List<Fee> _fees =
          feeObjsJson.map((tagJson) => Fee.fromJson(tagJson)).toList();

      return Trade(
        json['price'] as String,
        Pay.fromJson(json['pay']),
        Receive.fromJson(json['receive']),
        _fees,
        json['tradeDate'] as String,
        json['executedAt'] as String,
      );
    } else {
      return Trade(
        json['price'] as String,
        Pay.fromJson(json['pay']),
        Receive.fromJson(json['receive']),
        null,
        json['tradeDate'] as String,
        json['executedAt'] as String,
      );
    }
  }

  @override
  String toString() {
    return '{ $price, $pay, $receive, $fees, $tradeDate, $executedAt }';
  }
}

class Pay {
  String currency;
  String subtotal;
  String total;

  Pay(
    this.currency,
    this.subtotal,
    this.total,
  );

  factory Pay.fromJson(dynamic json) {
    return Pay(
      json['currency'] as String,
      json['subtotal'] as String,
      json['total'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $subtotal, $total }';
  }
}

class Receive {
  String currency;
  String amount;

  Receive(
    this.currency,
    this.amount,
  );

  factory Receive.fromJson(dynamic json) {
    return Receive(
      json['currency'] as String,
      json['amount'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount }';
  }
}

class Fee {
  String currency;
  String amount;
  String rateMethod;
  String rateValue;

  Fee(
    this.currency,
    this.amount,
    this.rateMethod,
    this.rateValue,
  );

  factory Fee.fromJson(dynamic json) {
    return Fee(
      json['currency'] as String,
      json['amount'] as String,
      json['rateMethod'] as String,
      json['rateValue'] as String,
    );
  }

  @override
  String toString() {
    return '{ $currency, $amount, $rateMethod, $rateValue }';
  }
}
