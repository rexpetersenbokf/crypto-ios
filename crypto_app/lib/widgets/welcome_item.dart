import 'package:flutter/material.dart';

import '../models/welcome.dart' as value;

class WelcomeItem extends StatelessWidget {
  final value.Welcome item;
  // ignore: use_key_in_widget_constructors
  const WelcomeItem(this.item);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 20,
      ),
      child: ListTile(
        dense: true,
        horizontalTitleGap: 0.0,
        leading: Image.asset(
          item.icon,
          height: 20,
          width: 20,
        ),
        title: Text(
          item.title,
          style: const TextStyle(
            fontFamily: 'Roboto',
            fontSize: 15,
          ),
        ),
      ),
    );
  }
}
