import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../config/constant.dart';
import '../../providers/account.dart';

class BitcoinValue extends StatefulWidget {
  const BitcoinValue({Key? key}) : super(key: key);

  @override
  _BitcoinValue createState() => _BitcoinValue();
}

class _BitcoinValue extends State<BitcoinValue> {
  var _isLoading = false;
  var _bitcoinPrice = {};

  Future<void> _callBitcoinPriceAPI() async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response = await Provider.of<Account>(context, listen: false)
          .getBitcoinPrice('BTC-USD');
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        _bitcoinPrice = response;
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    _callBitcoinPriceAPI();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(18),
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Color.fromARGB(255, 255, 255, 229),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'MARKET VALUE OF 1 BITCOIN:  ',
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontFamily: 'Roboto',
              fontSize: 11.0,
              fontWeight: FontWeight.normal,
            ),
          ),
          _isLoading == true
              ? const SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(),
                )
              : Text(
                  '\$${_bitcoinPrice['price']}',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontFamily: 'Roboto',
                    fontSize: 11.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
          IconButton(
            icon: const Icon(
              Icons.info_outline,
              size: 22,
            ),
            color: Theme.of(context).primaryColor,
            onPressed: () {
              showCustomDialog(context, 'Bitcoin Value',
                  'If you have a business name, trade name, DBA name or disregarded entity name, you may enter it here.');
            },
          ),
        ],
      ),
    );
  }
}
