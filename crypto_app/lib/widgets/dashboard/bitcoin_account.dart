import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../config/constant.dart';
import '../../config/globals.dart' as global;

import '../../providers/account.dart';

import '../../screens/account_home_screen.dart';
import '../../screens/buy_bitcoin_screen.dart';

class BitcoinAccount extends StatefulWidget {
  const BitcoinAccount({Key? key}) : super(key: key);

  @override
  _BitcoinAccount createState() => _BitcoinAccount();
}

class _BitcoinAccount extends State<BitcoinAccount> {
  var _isLoading = false;
  var _accountBalance = {};

  Future<void> _callBitcoinAccountBalanceAPI(String accountNumber) async {
    setState(() {
      _isLoading = true;
    });

    try {
      var response = await Provider.of<Account>(context, listen: false)
          .getBitcoinAccountBalance(accountNumber);
      var statusCode = response['status'];
      var message = response['title'];
      if (statusCode != null && apiErrorCode.contains(statusCode)) {
        showCustomDialog(context, 'Error', message);
      } else {
        var accountInfo = response['crypto'];
        _accountBalance = accountInfo[0];
      }
    } catch (error) {
      showCustomDialog(context, 'Error', errorMessage);
    }

    setState(() {
      _isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();

    var _accountNumber = global.userAccount['accountNumber'];
    _callBitcoinAccountBalanceAPI(_accountNumber);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.symmetric(vertical: 16),
      color: Theme.of(context).primaryColor,
      child: Column(
        children: [
          Row(
            children: [
              Image.asset(
                'assets/images/icTimer.png',
                height: 32,
                width: 32,
              ),
              const SizedBox(
                width: 8,
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          'Bitcoin',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Text(
                          'Account Balance',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontFamily: 'Roboto',
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    _isLoading == true
                        ? const CircularProgressIndicator(
                            color: Colors.white,
                          )
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                '\$${_accountBalance['marketValue']}',
                                textAlign: TextAlign.right,
                                style: const TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              Text(
                                '${_accountBalance['quantity']} ${_accountBalance['currency']}',
                                textAlign: TextAlign.right,
                                style: const TextStyle(
                                  fontFamily: 'Roboto',
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 32,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                  primary: Colors.white,
                  side: const BorderSide(color: Colors.white, width: 1.25),
                ),
                child: const Text(
                  'Buy Bitcoin',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(BuyBitcoinScreen.routeName,
                      arguments: UserAction.buy);
                },
              ),
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                  primary: Colors.white,
                  side: const BorderSide(color: Colors.white, width: 1.25),
                ),
                child: const Text(
                  'Sell Bitcoin',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pushNamed(BuyBitcoinScreen.routeName,
                      arguments: UserAction.sell);
                },
              ),
              OutlinedButton(
                style: OutlinedButton.styleFrom(
                  primary: Theme.of(context).primaryColor,
                  side: const BorderSide(color: Colors.white, width: 1.25),
                  backgroundColor: Colors.white,
                ),
                child: const Text(
                  'Bitcoin Account',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 13,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, AccountHomeScreen.routeName, (_) => true);
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
