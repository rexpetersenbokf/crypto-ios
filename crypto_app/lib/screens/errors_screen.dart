import 'package:flutter/material.dart';

import '../config/constant.dart';

import '../screens/unavailable_screen.dart';
import '../screens/account_home_screen.dart';

class ErrorsScreen extends StatelessWidget {
  const ErrorsScreen({Key? key}) : super(key: key);
  static const routeName = '/error';

  @override
  Widget build(BuildContext context) {
    final error = ModalRoute.of(context)!.settings.arguments as Error;
    String title = '';
    if (error == Error.ineligible) {
      title =
          'Sorry, you are not currently eligible for the Bitcoin Trading Platform, please check back later.';
    } else if (error == Error.unavailable) {
      title =
          'The Bitcoin Trading Platform is currently unavailable, please check back later.';
    } else if (error == Error.maintenance) {
      'Sorry, the Bitcoin Trading Platform is unavailable due to scheduled maintenance.';
    }

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        title: const Text('Future Bank'),
      ),
      body: Column(
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: SizedBox(
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/images/icEligibleCard.png',
                    width: 250,
                    height: 80,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 40,
                      horizontal: 16,
                    ),
                    child: Text(
                      title,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 23,
                        fontWeight: FontWeight.w300,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 110,
                    height: 56,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(
                          const Color.fromARGB(255, 0, 112, 228),
                        ),
                      ),
                      child: const Text(
                        'Done',
                        style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      onPressed: () {
                        if (error == Error.ineligible) {
                          Navigator.of(context).pushReplacementNamed(
                              UnavailableScreen.routeName);
                        } else if (error == Error.unavailable) {
                          Navigator.of(context).pushReplacementNamed(
                              ErrorsScreen.routeName,
                              arguments: Error.maintenance);
                        } else {
                          Navigator.pushNamedAndRemoveUntil(context,
                              AccountHomeScreen.routeName, (_) => true);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          SafeArea(
            bottom: true,
            child: Padding(
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: 'Bitcoin accounts and services provided by NYDIG. See ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    color: Color.fromARGB(255, 189, 189, 189),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Terms.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 189, 189, 189),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 16),
            ),
          ),
        ],
      ),
    );
  }
}
