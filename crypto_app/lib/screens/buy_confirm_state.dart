import 'package:flutter/material.dart';

import '../config/constant.dart';
import '../models/quote.dart';
import '../screens/account_home_screen.dart';

class BuyConfirmState extends StatelessWidget {
  const BuyConfirmState({Key? key}) : super(key: key);

  static const routeName = '/buy_confirm_state';

  @override
  Widget build(BuildContext context) {
    final arguments =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    var state = arguments['state'];
    var action = arguments['action'];
    var quote = Quote.fromJson(arguments['quote'] as Map<String, dynamic>);

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        title: const Text('Future Bank'),
      ),
      body: Column(
        children: <Widget>[
          if (state == BuyState.expired) ...[
            const QuoteExpired()
          ] else ...[
            BuySuccess(action, quote)
          ],
          SafeArea(
            bottom: true,
            child: Padding(
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text: 'Bitcoin accounts and services provided by NYDIG. See ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 11,
                    color: Color.fromARGB(255, 189, 189, 189),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'Terms.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 11,
                        color: Color.fromARGB(255, 189, 189, 189),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                  ],
                ),
              ),
              padding: const EdgeInsets.symmetric(horizontal: 16),
            ),
          ),
        ],
      ),
    );
  }
}

class QuoteExpired extends StatelessWidget {
  const QuoteExpired({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/icEligibleCard.png',
              width: 250,
              height: 80,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(
                vertical: 40,
                horizontal: 16,
              ),
              child: Text(
                'This quote has expired',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 23,
                  fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(
                left: 30,
                right: 30,
                bottom: 64,
                top: 24,
              ),
              child: Text(
                'Quotes are only good for 30 seconds. Refresh the quote to continue this purchase.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(
              width: 186,
              height: 56,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    const Color.fromARGB(255, 0, 112, 228),
                  ),
                ),
                child: const Text(
                  'Refresh Quote',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed(
                      BuyConfirmState.routeName,
                      arguments: {
                        'status': BuyState.success,
                      });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class BuySuccess extends StatelessWidget {
  final UserAction? action;
  final Quote quote;

  // ignore: use_key_in_widget_constructors
  const BuySuccess(this.action, this.quote);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image.asset(
              'assets/images/icEligibleCard.png',
              width: 250,
              height: 80,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 22,
                left: 16,
                right: 16,
              ),
              child: Text(
                (action == UserAction.buy || action == null)
                    ? 'Your buy order is complete'
                    : 'Your sell order is complete',
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontFamily: 'Roboto',
                  fontSize: 23,
                  fontWeight: FontWeight.w300,
                  color: Colors.white,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30,
                top: 48,
              ),
              child: RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                  text: (action == UserAction.buy || action == null)
                      ? 'You spent '
                      : 'You sold ',
                  style: const TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 16,
                    color: Colors.white,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: (action == UserAction.buy || action == null)
                          ? '\$100.00 USD'
                          : '0.00439813 BTC',
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                    TextSpan(
                      text: (action == UserAction.buy || action == null)
                          ? ' to purchase '
                          : ' and will receive ',
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                    TextSpan(
                      text: (action == UserAction.buy || action == null)
                          ? '0.01093376 BTC'
                          : '\$246.25',
                      style: const TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 16,
                        fontWeight: FontWeight.w700,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30,
                bottom: 44,
                top: 24,
              ),
              child: RichText(
                textAlign: TextAlign.center,
                text: const TextSpan(
                  text:
                      'A copy of your transaction receipt is now available inside your ',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 16,
                    color: Colors.white,
                  ),
                  children: <TextSpan>[
                    TextSpan(
                      text: 'account',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color.fromARGB(255, 4, 209, 184),
                        decoration: TextDecoration.underline,
                      ),
                    ),
                    TextSpan(
                      text: '.',
                      style: TextStyle(
                        fontFamily: 'Roboto',
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              width: 108,
              height: 56,
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(
                    const Color.fromARGB(255, 0, 112, 228),
                  ),
                ),
                child: const Text(
                  'Done',
                  style: TextStyle(
                    fontFamily: 'Roboto',
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, AccountHomeScreen.routeName, (_) => true);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
