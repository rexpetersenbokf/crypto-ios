import 'package:flutter/material.dart';
import '../../config/globals.dart' as global;

class NameImage extends StatelessWidget {
  const NameImage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(
              'Welcome back, ${global.user?.name ?? ''}',
              softWrap: true,
              style: const TextStyle(
                fontFamily: 'Roboto',
                fontSize: 28,
                fontWeight: FontWeight.w500,
                color: Colors.black,
              ),
            ),
          ),
          const CircleAvatar(
            radius: 32.0,
            backgroundImage:
                NetworkImage('https://via.placeholder.com/140x100'),
          )
        ],
      ),
    );
  }
}
