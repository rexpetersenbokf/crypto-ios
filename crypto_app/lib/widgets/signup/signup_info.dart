import 'package:flutter/material.dart';

class SignupInfo extends StatelessWidget {
  const SignupInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 48,
      ),
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        color: const Color.fromARGB(255, 240, 240, 240),
        border: Border.all(
          width: 1.0,
          color: const Color.fromARGB(255, 189, 189, 189),
        ),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: const Text(
        'THE INTERNAL REVENUE SERVICE DOES NOT REQUIRE YOUR CONSENT TO ANY PROVISION OF THIS DOCUMENT OTHER THAN THE CERTIFICATIONS REQUIRED TO AVOID BACKUP WITHHOLDING.',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontFamily: 'Roboto',
          fontSize: 13.0,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
