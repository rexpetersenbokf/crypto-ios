class QuoteModel {
  final String title;
  final String info;
  String? subinfo = '';

  QuoteModel({
    required this.title,
    required this.info,
    this.subinfo,
  });
}
