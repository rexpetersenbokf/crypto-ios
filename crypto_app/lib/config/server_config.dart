enum Environment { dev, staging, prod }

class Constants {
  static Map<String, dynamic>? _config = {};

  static void setEnvironment(Environment env) {
    switch (env) {
      case Environment.dev:
        _config = _Config.debugConstants;
        break;
      case Environment.staging:
        _config = _Config.qaConstants;
        break;
      case Environment.prod:
        _config = _Config.prodConstants;
        break;
    }
  }

  static get serverURL {
    return _config![_Config.serverURL];
  }

  static get serverEnvironment {
    return _config![_Config.serverEnvironment];
  }
}

class _Config {
  static const serverURL = "SERVER";
  static const serverEnvironment = "WHERE_AM_I";

  static Map<String, dynamic> debugConstants = {
    serverURL: "https://sandbox.nydig-uat.cloud/partners/v1/",
    serverEnvironment: "local",
  };

  static Map<String, dynamic> qaConstants = {
    serverURL: "https://staging1.example.com/",
    serverEnvironment: "staging",
  };

  static Map<String, dynamic> prodConstants = {
    serverURL: "https://itsallwidgets.com/",
    serverEnvironment: "prod"
  };
}
